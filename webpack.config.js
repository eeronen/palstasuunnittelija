const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: "./src/index.tsx",
    devtool: "inline-source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/,
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    "css-loader",
                    "sass-loader",
                ],
            },
        ],
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js", "scss"],
    },
    output: {
        filename: "[hash].main.js",
        path: path.resolve(__dirname, "dist"),
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[hash].styles.css",
        }),
        new HtmlWebpackPlugin({ template: "./src/index.html" }),
        new CopyWebpackPlugin({
            patterns: [
                {
                    from: path.join("src", "api"),
                    to: "api",
                },
                {
                    from: path.join("src", "assets"),
                    to: "assets",
                },
            ],
        }),
    ],
};
