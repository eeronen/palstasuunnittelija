export interface DrawingObjectTransferFormat {
    name: string;
    parent: string | null;
    x: number;
    y: number;
    orientation: number;
    type: string;
    shape_data: any;
    id?: number;
}
