import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Editor } from "./components/Editor";
import { store } from "./state/store";
import { Calendar } from "./components/Calendar";
import { Routes } from "./constants";

import "./CommonStyles.scss";
import { SideMenu } from "./components/SideMenu";

ReactDOM.render(
    <Provider store={store}>
        <div className="main-content">
            <BrowserRouter>
                <SideMenu />
                <Switch>
                    <Route path={Routes.Editor}>
                        <Editor />
                    </Route>
                    <Route path={Routes.Calendar}>
                        <Calendar />
                    </Route>
                    <Route path={Routes.Home}>
                        <Editor />
                    </Route>
                </Switch>
            </BrowserRouter>
        </div>
    </Provider>,
    document.getElementById("app-root")
);
