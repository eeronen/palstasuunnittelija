import * as React from "react";
import { executeCommand } from "./commands";
import "./Commander.scss";

interface HistoryItem {
    command: string;
    output?: string;
}

export const CommanderInput: React.FC<{}> = (props) => {
    const [history, setHistory] = React.useState([] as HistoryItem[]);
    const [historyIndex, setHistoryIndex] = React.useState(-1);
    const [input, setInput] = React.useState("");
    const inputRef = React.useRef(null as null | HTMLInputElement);
    React.useEffect(() => {
        if (historyIndex >= 0) {
            const commandEndLocation = history[historyIndex].command.length;
            inputRef.current.setSelectionRange(
                commandEndLocation,
                commandEndLocation
            );
        }
    }, [historyIndex]);
    React.useEffect(() => {
        const anyKeyPressedHandler = (event: KeyboardEvent) => {
            if (
                inputRef.current !== document.activeElement &&
                event.key === "Shift"
            ) {
                inputRef.current.focus();
            }
        };
        document.addEventListener("keydown", anyKeyPressedHandler);
        return () => {
            document.removeEventListener("keydown", anyKeyPressedHandler);
        };
    }, []);
    return (
        <div className="commander__container">
            <div className="command-history__container">
                {history.map((historyItem, index) => (
                    <div key={index} className="command-history__item">
                        <div className="command-history__command">
                            {historyItem.command}
                        </div>
                        {historyItem.output && (
                            <div className="command-history__output">
                                {historyItem.output}
                            </div>
                        )}
                    </div>
                ))}
            </div>
            <input
                value={input}
                className="commander__input"
                ref={inputRef}
                onKeyDown={(e) => {
                    if (e.key === "Enter") {
                        const historyEntry: HistoryItem = { command: input };
                        try {
                            executeCommand(input);
                        } catch (error) {
                            historyEntry.output = error.message;
                        }
                        setHistory([historyEntry, ...history]);
                        setHistoryIndex(-1);
                        setInput("");
                        e.stopPropagation();
                    } else if (
                        e.key === "ArrowUp" &&
                        historyIndex < history.length - 1
                    ) {
                        setHistoryIndex(historyIndex + 1);
                        setInput(history[historyIndex + 1].command);
                    } else if (e.key === "ArrowDown") {
                        if (historyIndex > 0) {
                            setHistoryIndex(historyIndex - 1);
                            setInput(history[historyIndex - 1].command);
                        } else {
                            setHistoryIndex(-1);
                            setInput("");
                        }
                    }
                }}
                onChange={(event) => {
                    setInput(event.target.value);
                }}
            />
        </div>
    );
};
