import { drawingActions } from "../../../state/drawingSlice/drawingSlice";
import { addObject } from "../../../state/drawingSlice/operations";
import {
    getObjectNames,
    getSelectedObjects,
} from "../../../state/drawingSlice/selectors";
import { store } from "../../../state/store";

const enum CommandParameterType {
    String,
    Number,
}

interface CommandParams {
    name: string;
    description: string;
    isOptional?: boolean;
    type: CommandParameterType;
}

interface Command {
    canExecute: () => boolean;
    execute: (params: any[]) => void;
    params: CommandParams[];
}

export const executeCommand = (commandString: string) => {
    let { commandName, params } = parseCommand(commandString);

    if (ALIASES[commandName] !== undefined) {
        commandName = ALIASES[commandName];
    }
    const command = COMMANDS[commandName];
    if (command === undefined) {
        throw new Error(`Komentoa ${commandName} ei tunnistettu`);
    }
    if (!command.canExecute()) {
        throw new Error("Komentoa ei voida suorittaa");
    }
    validateParams(command, params);

    command.execute(params);
};

export const validateParams = (command: Command, params: any[]) => {
    params.forEach((param, index) => {
        if (
            command.params[index].type === CommandParameterType.Number &&
            isNaN(param)
        ) {
            throw new Error(`${param} ei ole numero`);
        }
    });
};

export const parseCommand = (commandString: string) => {
    const [_, commandName, params] = commandString.match(
        /^([a-zA-ZöäÖÄ]*)[\s\(\[\{](.*)[\s\(\[\{]*/
    ) || [undefined, commandString, ""];
    let paramParts;
    if (/,/.test(params)) {
        paramParts = params.split(",");
    } else if (/\;/.test(params)) {
        paramParts = params.split(";");
    } else {
        // assume it's space separated
        paramParts = params.split(" ");
    }
    return {
        commandName: commandName.trim(),
        params: paramParts.map((part) => part.trim()),
    };
};

type Alias = string;

export const COMMANDS: { [key: string]: Command } = {
    move: {
        canExecute: () => true,
        execute: (params) => {
            const selected = getSelectedObjects(store.getState());
            store.dispatch(
                drawingActions.moveObject({
                    name: selected[0],
                    location: { x: params[0], y: params[1] },
                })
            );
        },
        params: [
            { name: "x", description: "", type: CommandParameterType.Number },
            { name: "y", description: "", type: CommandParameterType.Number },
        ],
    },
    rotate: {
        canExecute: () => true,
        execute: (params) => {
            const selected = getSelectedObjects(store.getState());
            store.dispatch(
                drawingActions.setOrientation({
                    name: selected[0],
                    orientation: params[0],
                })
            );
        },
        params: [
            {
                name: "asteet",
                description: "",
                type: CommandParameterType.Number,
            },
        ],
    },
    rename: {
        canExecute: () => true,
        execute: (params) => {
            const selected = getSelectedObjects(store.getState());
            store.dispatch(
                drawingActions.renameObject({
                    oldName: selected[0],
                    newName: params[0],
                })
            );
        },
        params: [
            {
                name: "nimi",
                description: "",
                type: CommandParameterType.String,
            },
        ],
    },
    select: {
        canExecute: () => true,
        execute: (params) => {
            const objectNames = getObjectNames(store.getState());
            if (params.length > 0) {
                const notFoundObjects = params.filter(
                    (name) => !objectNames.includes(name)
                );
                if (notFoundObjects.length !== 0) {
                    throw new Error(
                        `Alueita ${notFoundObjects.join(", ")} ei löydy`
                    );
                }
            }
            store.dispatch(drawingActions.setSelectedObjects(params));
        },
        params: [
            {
                name: "nimi",
                description: "",
                type: CommandParameterType.String,
            },
        ],
    },
    add: {
        canExecute: () => true,
        execute: (params) => {
            // TODO: add some other type
            store.dispatch(addObject());
        },
        params: [
            {
                name: "tyyppi",
                description: "",
                type: CommandParameterType.String,
            },
            {
                name: "määrä",
                description: "",
                type: CommandParameterType.String,
            },
        ],
    },
};

export const ALIASES: { [key: string]: Alias } = {
    m: "move",
    w: "move",
    mv: "move",
    liikuta: "move",
    siirrä: "move",
    r: "rotate",
    kierrä: "rotate",
    nimeä: "rename",
    rn: "rename",
    s: "select",
    valitse: "select",
    lisää: "add",
    a: "add",
};
