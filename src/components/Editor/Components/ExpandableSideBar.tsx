import * as React from "react";
import { ArrowIcon } from "../../Icons/ArrowIcon";

import "./styles/ExpandableSideBar.scss";

export const enum Side {
    Left = "left",
    Right = "right",
}

export const ExpandableSideBar: React.FC<{
    side: Side;
    isExpanded: boolean;
    setExpanded: (willExpand: boolean) => void;
}> = (props) => {
    const { side, isExpanded, setExpanded } = props;
    const baseRotation = side === Side.Left ? -180 : 0;
    return (
        <div className={`side-bar__bar side-bar__bar--${side}`}>
            <div
                className={`side-bar__expand-icon-container side-bar__expand-icon-container--${side}`}
                onClick={() => setExpanded(!isExpanded)}
            >
                <div
                    className="side-bar__expand-icon"
                    style={{
                        transform: `rotate(${
                            isExpanded ? baseRotation : baseRotation + 180
                        }deg)`,
                    }}
                >
                    <ArrowIcon />
                </div>
            </div>
        </div>
    );
};
