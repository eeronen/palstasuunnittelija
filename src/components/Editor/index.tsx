import * as React from "react";
import { Canvas } from "./Canvas";
import { Navigator } from "./Navigator";
import { FunctionBar } from "./FunctionBar";

import "./EditorStyles.scss";
import { PropertyView } from "./PropertyView";
import { handleKeyboardEvent } from "./KeyboardHandler";
import { CommanderInput } from "./Commander";

export const Editor: React.FC<{}> = () => {
    return (
        <div
            className="editor__container"
            onKeyDown={handleKeyboardEvent}
            tabIndex={0}
        >
            <FunctionBar />
            <div className="editor__main-content">
                <Navigator />
                <div className="editor__canvas-container">
                    <Canvas />
                    <CommanderInput />
                </div>
                <PropertyView />
            </div>
        </div>
    );
};
