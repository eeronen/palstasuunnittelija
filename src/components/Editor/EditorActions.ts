import { Dispatch } from "redux";
import { commitDrawing } from "../../services/drawings";
import { drawingActions } from "../../state/drawingSlice/drawingSlice";
import {
    getCopiedObjects,
    getDrawingObjectList,
    getSelectedObjects,
} from "../../state/drawingSlice/selectors";
import { store, StoreState } from "../../state/store";

export interface EditorAction {
    canExecute: (state: StoreState) => boolean;
    execute: (dispatch: Dispatch) => void;
}

export type ExecuteEditorAction = Pick<EditorAction, "execute">;

export const copyAction: EditorAction = {
    canExecute: (state) => getSelectedObjects(state).length !== 0,
    execute: (dispatch) =>
        dispatch(
            drawingActions.setCopiedObject(getSelectedObjects(store.getState()))
        ),
};

export const pasteAction: EditorAction = {
    canExecute: (state) => getCopiedObjects(state).length !== 0,
    execute: (dispatch) => dispatch(drawingActions.pasteObjects()),
};

export const deleteAction: EditorAction = {
    canExecute: (state) => getSelectedObjects(state).length !== 0,
    execute: (dispatch) => dispatch(drawingActions.removeSelectedObject()),
};

export const saveAction: EditorAction = {
    canExecute: (_) => true,
    execute: async (dispatch) => {
        await commitDrawing(getDrawingObjectList(store.getState()));
    },
};
