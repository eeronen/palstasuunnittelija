import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { drawingActions } from "../../../state/drawingSlice/drawingSlice";
import {
    getObject,
    getSelectedObjects,
} from "../../../state/drawingSlice/selectors";
import { ObjectType, PlantObject } from "../../../state/drawingSlice/types";
import { getPlantCount } from "../../../state/selectors";
import { StoreState } from "../../../state/store";
import { BlurTriggeredInput, ObjectTypePicker } from "./inputs";
import { SeedPicker } from "./SeedPicker";

interface OwnProps {}

interface StateProps {
    name: string;
    objectData: PlantObject;
    count: number;
}

interface DispatchProps {
    changeName: (oldName: string, newName: string) => void;
    changeType: (objectName: string, type: ObjectType) => void;
    changeOrientation: (name: string, orientation: number) => void;
    changeHeight: (name: string, height: number) => void;
    changeWidth: (name: string, width: number) => void;
    changeCount: (name: string, count: number | undefined) => void;
}

type PropertyViewProps = OwnProps & StateProps & DispatchProps;

const UnconnectedPlantPropertyView: React.FC<PropertyViewProps> = (props) => {
    return (
        <>
            <BlurTriggeredInput
                currentValue={props.name}
                propertyName="Nimi"
                commitValue={(newName: string) =>
                    props.changeName(props.name, newName)
                }
            />
            <ObjectTypePicker
                setType={(type) => props.changeType(props.name, type)}
                currentType={props.objectData.type}
            />
            {props.objectData.type === ObjectType.Plant && (
                <SeedPicker name={props.name} />
            )}
            <BlurTriggeredInput
                currentValue={props.objectData.orientation}
                propertyName="Kierto"
                commitValue={(newOrientation: number) =>
                    props.changeOrientation(props.name, newOrientation)
                }
            />
            <BlurTriggeredInput
                currentValue={props.objectData.width}
                propertyName="Leveys"
                commitValue={(width: number) =>
                    props.changeWidth(props.name, width)
                }
            />
            <BlurTriggeredInput
                currentValue={props.objectData.height}
                propertyName="Korkeus"
                commitValue={(height: number) =>
                    props.changeHeight(props.name, height)
                }
            />
            <BlurTriggeredInput
                propertyName="Kasvien määrä"
                currentValue={props.count}
                commitValue={(count: number | string) => {
                    if (!count) {
                        props.changeCount(props.name, undefined);
                    } else {
                        props.changeCount(props.name, Number(count));
                    }
                }}
            />
        </>
    );
};

const mapStateToProps = (state: StoreState, ownProps: OwnProps): StateProps => {
    const name = getSelectedObjects(state)[0];
    const objectData = getObject(state, name) as PlantObject;
    const count = getPlantCount(state, name);
    return {
        name,
        objectData,
        count,
    };
};

const mapDispatchToProps = (
    dispatch: Dispatch,
    ownProps: OwnProps
): DispatchProps => ({
    changeName: (oldName, newName) =>
        dispatch(drawingActions.renameObject({ oldName, newName })),
    changeType: (objectName, type) =>
        dispatch(
            drawingActions.changeObjectData({
                name: objectName,
                newData: { type },
            })
        ),
    changeOrientation: (name, orientation) =>
        dispatch(drawingActions.setOrientation({ name, orientation })),
    changeHeight: (name, height) =>
        dispatch(drawingActions.setRectangleDimensions({ name, height })),
    changeWidth: (name, width) =>
        dispatch(drawingActions.setRectangleDimensions({ name, width })),
    changeCount: (name, count) =>
        dispatch(drawingActions.changeCount({ name, count })),
});

export const PlantPropertyView = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedPlantPropertyView);
