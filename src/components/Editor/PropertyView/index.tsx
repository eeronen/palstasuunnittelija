import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import {
    getObjectType,
    getSelectedObjects,
} from "../../../state/drawingSlice/selectors";
import {
    isRectangleObject,
    ObjectType,
} from "../../../state/drawingSlice/types";
import { StoreState } from "../../../state/store";
import { ExpandableSideBar, Side } from "../Components/ExpandableSideBar";
import { AreaPropertyView } from "./AreaPropertyView";
import { GeneralPropertyView } from "./GeneralPropertyView";
import { PlantPropertyView } from "./PlantPropertyView";

import "./PropertyView.scss";

interface OwnProps {}

interface StateProps {
    name: string;
    type: ObjectType;
}

interface DispatchProps {}

type PropertyViewProps = OwnProps & StateProps & DispatchProps;

const UnconnectedPropertyView: React.FC<PropertyViewProps> = (props) => {
    const [isExpanded, setExpanded] = React.useState(true);
    return (
        <div
            className="property-view__container"
            style={{ width: isExpanded ? "200px" : "0px" }}
        >
            <ExpandableSideBar
                side={Side.Right}
                isExpanded={isExpanded}
                setExpanded={setExpanded}
            />
            <div className="property-view__content">
                {props.name ? (
                    <>
                        <div className="property-view__header">
                            {props.name}
                        </div>
                        <div className="property-view__inputs">
                            <PropertyViewInputs {...props} />
                        </div>
                    </>
                ) : (
                    <GeneralPropertyView />
                )}
            </div>
        </div>
    );
};

const PropertyViewInputs: React.FC<PropertyViewProps> = (props) => {
    if (isRectangleObject(props.type)) {
        return <PlantPropertyView />;
    } else {
        return <AreaPropertyView />;
    }
};

const mapStateToProps = (state: StoreState, ownProps: OwnProps): StateProps => {
    const name = getSelectedObjects(state)[0];
    const type = name !== undefined ? getObjectType(state, name) : undefined;
    return {
        name,
        type,
    };
};

const mapDispatchToProps = (
    dispatch: Dispatch,
    ownProps: OwnProps
): DispatchProps => ({});

export const PropertyView = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedPropertyView);
