import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { getAllPlantCounts } from "../../../state/selectors";
import { StoreState } from "../../../state/store";

interface OwnProps {}

interface StateProps {
    plantCounts: [string, number][];
}

interface DispatchProps {}

type PropertyViewProps = OwnProps & StateProps & DispatchProps;

const UnconnectedGeneralPropertyView: React.FC<PropertyViewProps> = (props) => {
    return (
        <>
            <ul className="plant-count__list">
                {props.plantCounts.map(([plantName, count]) => (
                    <li key={plantName} className="plant-count__item">
                        <div className="plant-count__name">{plantName}</div>
                        <div className="plant-count__count">{count}</div>
                    </li>
                ))}
            </ul>
        </>
    );
};

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    plantCounts: Object.entries(getAllPlantCounts(state)),
});

const mapDispatchToProps = (
    dispatch: Dispatch,
    ownProps: OwnProps
): DispatchProps => ({});

export const GeneralPropertyView = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedGeneralPropertyView);
