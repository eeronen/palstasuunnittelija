import * as React from "react";
import { ObjectType } from "../../../state/drawingSlice/types";

export const BlurTriggeredInput: React.FC<{
    currentValue: string | number;
    propertyName: string | number;
    commitValue: (value: string | number) => void;
}> = (props) => {
    const [inputValue, setInputValue] = React.useState(props.currentValue);
    React.useEffect(() => {
        setInputValue(props.currentValue);
    }, [props.currentValue]);
    const inputRef = React.useRef<HTMLInputElement>(null);
    return (
        <div className="input__container">
            <label className="input__label">{props.propertyName}</label>
            <input
                className="input__input"
                ref={inputRef}
                value={inputValue}
                onChange={(event) => setInputValue(event.target.value)}
                onBlur={() => props.commitValue(inputValue)}
                onKeyDown={(event) => {
                    if (event.key === "Enter" && inputRef !== null) {
                        inputRef.current.blur();
                    }
                }}
            />
        </div>
    );
};

export const ObjectTypePicker: React.FC<{
    setType: (type: ObjectType) => void;
    currentType: ObjectType;
}> = (props) => {
    return (
        <div className="input__container">
            <label className="input__label">Tyyppi</label>
            <select
                className="input__input"
                value={props.currentType}
                onChange={(event) =>
                    props.setType(event.target.value as ObjectType)
                }
            >
                {Object.values(ObjectType).map((type) => (
                    <option value={type} key={type}>
                        {type}
                    </option>
                ))}
            </select>
        </div>
    );
};

export const ReadOnlyInput: React.FC<{
    value: string | number;
    propertyName: string;
}> = (props) => {
    return (
        <div className="input__container">
            <label className="input__label">{props.propertyName}</label>
            <input className="input__input" value={props.value} readOnly />
        </div>
    );
};
