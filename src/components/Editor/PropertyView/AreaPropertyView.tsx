import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { drawingActions } from "../../../state/drawingSlice/drawingSlice";
import {
    getObject,
    getSelectedObjects,
} from "../../../state/drawingSlice/selectors";
import { DrawingObject, ObjectType } from "../../../state/drawingSlice/types";
import { StoreState } from "../../../state/store";
import { BlurTriggeredInput, ObjectTypePicker } from "./inputs";

interface OwnProps {}

interface StateProps {
    name: string;
    objectData: DrawingObject;
}

interface DispatchProps {
    changeName: (oldName: string, newName: string) => void;
    changeType: (objectName: string, type: ObjectType) => void;
    changeOrientation: (name: string, orientation: number) => void;
}

type PropertyViewProps = OwnProps & StateProps & DispatchProps;

const UnconnectedAreaPropertyView: React.FC<PropertyViewProps> = (props) => {
    return (
        <>
            <BlurTriggeredInput
                currentValue={props.name}
                propertyName="Nimi"
                commitValue={(newName: string) =>
                    props.changeName(props.name, newName)
                }
            />
            <ObjectTypePicker
                setType={(type) => props.changeType(props.name, type)}
                currentType={props.objectData.type}
            />
            <BlurTriggeredInput
                currentValue={props.objectData.orientation}
                propertyName="Kierto"
                commitValue={(newOrientation: number) =>
                    props.changeOrientation(props.name, newOrientation)
                }
            />
        </>
    );
};

const mapStateToProps = (state: StoreState, ownProps: OwnProps): StateProps => {
    const name = getSelectedObjects(state)[0];
    const objectData = getObject(state, name);
    return {
        name,
        objectData,
    };
};

const mapDispatchToProps = (
    dispatch: Dispatch,
    ownProps: OwnProps
): DispatchProps => ({
    changeName: (oldName, newName) =>
        dispatch(drawingActions.renameObject({ oldName, newName })),
    changeType: (objectName, type) =>
        dispatch(
            drawingActions.changeObjectData({
                name: objectName,
                newData: { type },
            })
        ),

    changeOrientation: (name, orientation) =>
        dispatch(drawingActions.setOrientation({ name, orientation })),
});

export const AreaPropertyView = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedAreaPropertyView);
