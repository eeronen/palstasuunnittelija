import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { drawingActions } from "../../../state/drawingSlice/drawingSlice";
import { getPlantName } from "../../../state/drawingSlice/selectors";
import {
    getRowMargin,
    getSeedNames,
} from "../../../state/SeedsSlice/selectors";
import { store, StoreState } from "../../../state/store";

interface OwnProps {
    name: string;
}

interface StateProps {
    seeds: string[];
    selectedSeed: string;
}

interface DispatchProps {
    setSeed: (seed: string) => void;
}

type SeedPickerProps = OwnProps & StateProps & DispatchProps;

const UnconnectedSeedPicker: React.FC<SeedPickerProps> = (props) => {
    return (
        <div className="input__container">
            <label className="input__label">Tyyppi</label>
            <select
                className="input__input"
                value={props.selectedSeed}
                onChange={(event) => props.setSeed(event.target.value)}
            >
                {Object.values(props.seeds).map((seed) => (
                    <option value={seed} key={seed}>
                        {seed}
                    </option>
                ))}
            </select>
        </div>
    );
};

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    seeds: getSeedNames(state),
    selectedSeed: getPlantName(state, ownProps.name),
});

const mapDispatchToProps = (
    dispatch: Dispatch,
    ownProps: OwnProps
): DispatchProps => ({
    setSeed: (seed) => {
        const rowWidth = getRowMargin(store.getState(), seed);
        dispatch(
            drawingActions.changeObjectData({
                name: ownProps.name,
                newData: { width: rowWidth, plant: seed },
            })
        );
    },
});

export const SeedPicker = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedSeedPicker);
