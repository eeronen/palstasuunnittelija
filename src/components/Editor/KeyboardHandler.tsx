import * as React from "react";
import { store } from "../../state/store";
import {
    copyAction,
    deleteAction,
    EditorAction,
    pasteAction,
} from "./EditorActions";

const KEYBOARD_SHORTCUTS: { [key: string]: EditorAction } = {
    "ctrl+c": copyAction,
    "ctrl+v": pasteAction,
    Delete: deleteAction,
};

export const handleKeyboardEvent = (event: React.KeyboardEvent) => {
    const keycode = (event.ctrlKey ? "ctrl+" : "") + event.key;
    const shortcut = KEYBOARD_SHORTCUTS[keycode];
    if (shortcut && shortcut.canExecute(store.getState())) {
        shortcut.execute(store.dispatch);
        event.stopPropagation();
        event.preventDefault();
    }
};
