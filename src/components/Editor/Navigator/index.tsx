import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { drawingActions } from "../../../state/drawingSlice/drawingSlice";
import {
    getSelectedObjects,
    getStructure,
    StructureObject,
} from "../../../state/drawingSlice/selectors";
import { StoreState } from "../../../state/store";
import { ExpandableSideBar, Side } from "../Components/ExpandableSideBar";

import "./Navigator.scss";

interface OwnProps {}

interface StateProps {
    structure: StructureObject;
    selectedAreas: string[];
}

interface DispatchProps {
    setAreaParent: (name: string, parent: string) => void;
    selectArea: (name: string) => void;
}

type NavigatorProps = OwnProps & StateProps & DispatchProps;

const UnconnectedNavigator: React.FC<NavigatorProps> = (props) => {
    const [isExpanded, setExpanded] = React.useState(true);
    return (
        <div
            className="navigator__container"
            style={{ width: isExpanded ? "200px" : "0px" }}
            onDrop={(event) => {
                const draggedName = event.dataTransfer.getData("text");
                props.setAreaParent(draggedName, null);
                event.stopPropagation();
                event.preventDefault();
            }}
            onDragOver={(event) => {
                event.preventDefault();
            }}
        >
            <ObjectList
                structure={props.structure}
                setAreaParent={props.setAreaParent}
                selectArea={props.selectArea}
                selectedAreas={props.selectedAreas}
                depth={0}
            />
            <ExpandableSideBar
                side={Side.Left}
                isExpanded={isExpanded}
                setExpanded={setExpanded}
            />
        </div>
    );
};

const ObjectList: React.FC<{
    structure: StructureObject;
    setAreaParent: (name: string, parent: string) => void;
    selectArea: (name: string) => void;
    selectedAreas: string[];
    depth: number;
}> = (props) => {
    return (
        <ul className="navigator-list__list">
            {Object.keys(props.structure).map((name) => (
                <li
                    key={name}
                    className={`navigator-list__item${
                        props.selectedAreas.includes(name)
                            ? " navigator-list__item--selected"
                            : ""
                    }`}
                    draggable="true"
                    onDragStart={(event) => {
                        event.dataTransfer.setData("text", name);
                        event.stopPropagation();
                    }}
                    onDrop={(event) => {
                        const draggedName = event.dataTransfer.getData("text");
                        props.setAreaParent(draggedName, name);
                        event.stopPropagation();
                        event.preventDefault();
                    }}
                    onDragOver={(event) => {
                        event.preventDefault();
                    }}
                    onClick={(event) => {
                        props.selectArea(name);
                        event.stopPropagation();
                    }}
                >
                    <div
                        className="navigator-list__item-name"
                        style={{ paddingLeft: `${props.depth * 5}px` }}
                    >
                        {name}
                    </div>
                    {Object.keys(props.structure[name]).length !== 0 && (
                        <ObjectList
                            structure={props.structure[name]}
                            setAreaParent={props.setAreaParent}
                            selectArea={props.selectArea}
                            selectedAreas={props.selectedAreas}
                            depth={props.depth + 1}
                        />
                    )}
                </li>
            ))}
        </ul>
    );
};

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    structure: getStructure(state),
    selectedAreas: getSelectedObjects(state),
});

const mapDispatchToProps = (
    dispatch: Dispatch,
    ownProps: OwnProps
): DispatchProps => ({
    setAreaParent: (name, parent) =>
        dispatch(drawingActions.setObjectParent({ name, parent })),
    selectArea: (name) => dispatch(drawingActions.setSelectedObjects([name])),
});

export const Navigator = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedNavigator);
