import { Action } from "@reduxjs/toolkit";
import * as React from "react";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { addObject } from "../../../state/drawingSlice/operations";
import { ObjectType } from "../../../state/drawingSlice/types";
import { StoreState } from "../../../state/store";

import "./FunctionBar.scss";

interface OwnProps {}

interface StateProps {}

interface DispatchProps {
    addArea: (type: ObjectType) => void;
}

type FunctionBarProps = OwnProps & StateProps & DispatchProps;

const UnconnectedAddAreaFunctions: React.FC<FunctionBarProps> = (props) => {
    const makeAddAreaFunction = (type: ObjectType) => () => props.addArea(type);
    const [selectedType, setSelectedType] = React.useState(ObjectType.Base);
    const [isExpanded, setExpanded] = React.useState(false);
    return (
        <>
            <div className="add-area__container">
                <div
                    className="add-area__text"
                    onClick={makeAddAreaFunction(selectedType)}
                >
                    Lisää {selectedType.toLowerCase()}
                </div>
                {isExpanded ? (
                    <div
                        className="add-area__button"
                        onClick={() => setExpanded(false)}
                    >
                        -
                    </div>
                ) : (
                    <div
                        className="add-area__button"
                        onClick={() => setExpanded(true)}
                    >
                        +
                    </div>
                )}
                {isExpanded && (
                    <div className="add-area__list">
                        {Object.values(ObjectType).map((type) => (
                            <div
                                className="add-area__list-item"
                                onClick={() => {
                                    setSelectedType(type);
                                    setExpanded(false);
                                    props.addArea(type);
                                }}
                                key={type}
                            >
                                {type}
                            </div>
                        ))}
                    </div>
                )}
            </div>
        </>
    );
};

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({});

const mapDispatchToProps = (
    dispatch: ThunkDispatch<StoreState, null, Action>,
    ownProps: OwnProps
): DispatchProps => ({
    addArea: (type: ObjectType) => dispatch(addObject(type)),
});

export const AddAreaFunctions = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedAddAreaFunctions);
