import * as React from "react";
import { connect } from "react-redux";
import { Action } from "redux";
import { ThunkDispatch } from "redux-thunk";
import { StoreState } from "../../../state/store";
import { copyAction, pasteAction, saveAction } from "../EditorActions";
import { AddAreaFunctions } from "./AddAreaFunctions";

import "./FunctionBar.scss";

interface OwnProps {}

interface StateProps {
    canCopy: boolean;
    canPaste: boolean;
    canSave: boolean;
}

interface DispatchProps {
    copy: () => void;
    paste: () => void;
    save: () => void;
}

type FunctionBarProps = OwnProps & StateProps & DispatchProps;

const UnconnectedFunctionBar: React.FC<FunctionBarProps> = (props) => {
    return (
        <div className="function-bar__container">
            <div className="function-bar__items">
                <AddAreaFunctions />
            </div>
            <div className="function-bar__buttons">
                <div className="function-bar__button" onClick={props.save}>
                    Tallenna
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    canCopy: copyAction.canExecute(state),
    canPaste: copyAction.canExecute(state),
    canSave: saveAction.canExecute(state),
});

const mapDispatchToProps = (
    dispatch: ThunkDispatch<StoreState, null, Action>,
    ownProps: OwnProps
): DispatchProps => ({
    copy: () => copyAction.execute(dispatch),
    paste: () => pasteAction.execute(dispatch),
    save: () => saveAction.execute(dispatch),
});

export const FunctionBar = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedFunctionBar);
