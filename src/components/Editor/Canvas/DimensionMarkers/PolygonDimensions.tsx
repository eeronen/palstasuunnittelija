import * as React from "react";
import { connect } from "react-redux";
import { getObjectPoints } from "../../../../state/drawingSlice/selectors";
import { Coordinates } from "../../../../state/drawingSlice/types";
import { StoreState } from "../../../../state/store";

interface OwnProps {
    name: string;
}

interface StateProps {
    points: Coordinates[];
}

type DimensionsProps = OwnProps & StateProps;

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    points: getObjectPoints(state, ownProps.name),
});

export const UnconnectedPolygonDimensions: React.FC<DimensionsProps> = (
    props
) => (
    <g className="drawing-area__dimensions">
        {getPolygonSides(props.points).map((side, index) => (
            <text
                className="drawing-area__dimensions-text"
                fill="black"
                key={index}
                x={side.center.x}
                y={side.center.y}
                transform={`rotate(${side.orientation},${side.center.x},${side.center.y}) translate(-20,0)`}
            >
                {side.length} cm
            </text>
        ))}
    </g>
);

const getPolygonSides = (points: Coordinates[]) =>
    points.map((point, index) => {
        let point1 = point;
        let point2 = index < points.length - 1 ? points[index + 1] : points[0];
        return {
            length: Math.round(
                Math.sqrt(
                    Math.abs(point1.x - point2.x) ** 2 +
                        Math.abs(point1.y - point2.y) ** 2
                )
            ),
            center: {
                x: (point1.x + point2.x) / 2,
                y: (point1.y + point2.y) / 2,
            },
            orientation:
                Math.atan((point1.y - point2.y) / (point1.x - point2.x)) *
                (360 / (2 * Math.PI)),
        };
    });

export const PolygonDimensions = connect(mapStateToProps)(
    UnconnectedPolygonDimensions
);
