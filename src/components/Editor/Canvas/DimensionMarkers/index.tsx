import * as React from "react";
import { connect } from "react-redux";
import { getObjectType } from "../../../../state/drawingSlice/selectors";
import {
    ObjectType,
    isPolygonObject,
} from "../../../../state/drawingSlice/types";
import { StoreState } from "../../../../state/store";
import { PolygonDimensions } from "./PolygonDimensions";
import "./DimensionMarkers.scss";

interface OwnProps {
    name: string;
}

interface StateProps {
    type: ObjectType;
}

type DimensionMarkerProps = OwnProps & StateProps;

const UnconnectedDimensionMarker: React.FC<DimensionMarkerProps> = (props) => {
    if (isPolygonObject(props.type)) {
        return <PolygonDimensions name={props.name} />;
    } else {
        return null;
    }
};

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    type: getObjectType(state, ownProps.name),
});

export const DimensionMarker = connect(mapStateToProps)(
    UnconnectedDimensionMarker
);
