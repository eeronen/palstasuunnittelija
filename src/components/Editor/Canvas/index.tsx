import * as React from "react";
import { connect } from "react-redux";
import {
    getObjectNames,
    getPanPosition,
    getZoomLevel,
} from "../../../state/drawingSlice/selectors";
import { Coordinates } from "../../../state/drawingSlice/types";
import { StoreState } from "../../../state/store";
import { Area } from "./Area";
import { CanvasContainer } from "./CanvasContainer";

import "./Canvas.scss";
import { SnapLines } from "./DragHandles/SnapLines";

interface StateProps {
    areas: string[];
    zoomLevel: number;
    panPosition: Coordinates;
}

type CanvasProps = StateProps;

export const UnconnectedCanvas: React.FC<CanvasProps> = (props) => {
    return (
        <CanvasContainer>
            <svg
                className="drawing-canvas__container"
                style={{ transformOrigin: "left top" }}
                transform={`translate(${props.panPosition.x},${props.panPosition.y}) scale(${props.zoomLevel})`}
            >
                <rect
                    width="100%"
                    height="100%"
                    className="drawing-canvas__background"
                />
                {props.areas.map((area) => (
                    <Area name={area} key={area} />
                ))}
                <SnapLines />
            </svg>
        </CanvasContainer>
    );
};

const mapStateToProps = (state: StoreState): StateProps => ({
    areas: getObjectNames(state),
    zoomLevel: getZoomLevel(state),
    panPosition: getPanPosition(state),
});

export const Canvas = connect(mapStateToProps)(UnconnectedCanvas);
