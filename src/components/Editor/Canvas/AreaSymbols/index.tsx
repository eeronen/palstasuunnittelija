import * as React from "react";
import { connect, ConnectedComponent } from "react-redux";
import { getObjectType } from "../../../../state/drawingSlice/selectors";
import {
    ObjectType,
    isPolygonObject,
} from "../../../../state/drawingSlice/types";
import { StoreState } from "../../../../state/store";
import { PlantAreaSymbol } from "./PlantAreaSymbol";
import { PolygonAreaSymbol } from "./PolygonAreaSymbol";
import { RectangleAreaSymbol } from "./RectangleAreaSymbol";

import "./AreaSymbol.scss";

interface OwnProps {
    name: string;
}

interface StateProps {
    type: ObjectType;
}

type AreaSymbolProps = OwnProps & StateProps;

const UnconnectedAreaSymbol: React.FC<AreaSymbolProps> = (props) => {
    if (isPolygonObject(props.type)) {
        return <PolygonAreaSymbol name={props.name} />;
    } else if (props.type === ObjectType.Plant) {
        return <PlantAreaSymbol name={props.name} />;
    } else {
        return <RectangleAreaSymbol name={props.name} />;
    }
};

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    type: getObjectType(state, ownProps.name),
});

export const AreaSymbol: ConnectedComponent<
    typeof UnconnectedAreaSymbol,
    OwnProps
> = connect(mapStateToProps)(UnconnectedAreaSymbol);
