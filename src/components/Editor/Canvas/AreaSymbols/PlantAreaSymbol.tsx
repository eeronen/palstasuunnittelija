import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { drawingActions } from "../../../../state/drawingSlice/drawingSlice";
import {
    getWidth,
    getHeight,
    getPlantName,
    getSetPlantCount,
} from "../../../../state/drawingSlice/selectors";
import { ObjectType } from "../../../../state/drawingSlice/types";
import { getPlantMargin } from "../../../../state/SeedsSlice/selectors";
import { StoreState } from "../../../../state/store";
import { AREA_FILL } from "../utils";

interface OwnProps {
    name: string;
}
interface StateProps {
    type: ObjectType.Plant;
    width: number;
    height: number;
    plantMargin?: number;
    plantName?: string;
    forcedCount?: number;
}

interface DispatchProps {
    selectArea: () => void;
}

type AreaSymbolProps = OwnProps & StateProps & DispatchProps;

const UnconnectedPlantAreaSymbol: React.FC<AreaSymbolProps> = (props) => (
    <g
        onClick={(event) => {
            props.selectArea();
            event.stopPropagation();
        }}
    >
        <rect
            height={props.height}
            width={props.width}
            className={`drawing-symbol__area--${props.type.toLowerCase()}`}
            fill={AREA_FILL[props.type]}
        />
        <PlantMarkers {...props} />
        <text
            className="drawing-symbol__text"
            transform="rotate(90)"
            x="5"
            y={`${-props.width / 2 + 5}`}
        >
            {props.plantName || props.name}
        </text>
    </g>
);

const PlantMarkers: React.FC<AreaSymbolProps> = (props) => {
    const PlantMarkers = [];
    const plantMargin = !props.forcedCount
        ? props.plantMargin
        : props.height / props.forcedCount;
    for (
        let location = plantMargin / 2;
        location < props.height;
        location = location + plantMargin
    ) {
        PlantMarkers.push(
            <circle
                className="drawing-symbol__area--plant-marker"
                key={location}
                r={plantMargin / 2}
                cx={props.width / 2}
                cy={location}
            />
        );
    }
    return <>{...PlantMarkers}</>;
};

const mapStateToProps = (state: StoreState, ownProps: OwnProps): StateProps => {
    const plantName = getPlantName(state, ownProps.name);
    return {
        type: ObjectType.Plant,
        width: getWidth(state, ownProps.name),
        height: getHeight(state, ownProps.name),
        plantMargin: getPlantMargin(state, plantName),
        plantName,
        forcedCount: getSetPlantCount(state, ownProps.name),
    };
};

const mapDispatchToProps = (
    dispatch: Dispatch,
    ownProps: OwnProps
): DispatchProps => ({
    selectArea: () => {
        dispatch(drawingActions.setSelectedObjects([ownProps.name]));
    },
});

export const PlantAreaSymbol = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedPlantAreaSymbol);
