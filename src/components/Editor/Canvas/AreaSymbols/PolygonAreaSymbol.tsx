import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { drawingActions } from "../../../../state/drawingSlice/drawingSlice";
import {
    getObjectPoints,
    getObjectType,
} from "../../../../state/drawingSlice/selectors";
import { Coordinates, ObjectType } from "../../../../state/drawingSlice/types";
import { StoreState } from "../../../../state/store";
import { getPolygonPoints, AREA_FILL } from "../utils";

interface OwnProps {
    name: string;
}

interface StateProps {
    points: Coordinates[];
    type: ObjectType;
}

interface DispatchProps {
    selectArea: () => void;
}

type AreaSymbolProps = OwnProps & StateProps & DispatchProps;

const UnconnectedPolygonAreaSymbol: React.FC<AreaSymbolProps> = (props) => {
    const points = getPolygonPoints(props.points);
    return (
        <polygon
            points={points}
            className={`drawing-symbol__area drawing-symbol__area--${props.type.toLowerCase()}`}
            fill={AREA_FILL[props.type]}
            onClick={(event) => {
                props.selectArea();
                event.stopPropagation();
            }}
        />
    );
};

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    points: getObjectPoints(state, ownProps.name),
    type: getObjectType(state, ownProps.name),
});

const mapDispatchToProps = (
    dispatch: Dispatch,
    ownProps: OwnProps
): DispatchProps => ({
    selectArea: () => {
        dispatch(drawingActions.setSelectedObjects([ownProps.name]));
    },
});

export const PolygonAreaSymbol = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedPolygonAreaSymbol);
