import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { drawingActions } from "../../../../state/drawingSlice/drawingSlice";
import {
    getObjectType,
    getWidth,
    getHeight,
} from "../../../../state/drawingSlice/selectors";
import { RectangleObjectType } from "../../../../state/drawingSlice/types";
import { StoreState } from "../../../../state/store";
import { AREA_FILL } from "../utils";

interface OwnProps {
    name: string;
}
interface StateProps {
    type: RectangleObjectType;
    width: number;
    height: number;
    plantMargin?: number;
    plantName?: string;
    forcedCount?: number;
}

interface DispatchProps {
    selectArea: () => void;
}

type AreaSymbolProps = OwnProps & StateProps & DispatchProps;

const UnconnectedRectangleAreaSymbol: React.FC<AreaSymbolProps> = (props) => (
    <rect
        height={props.height}
        width={props.width}
        className={`drawing-symbol__area drawing-symbol__area--${props.type.toLowerCase()}`}
        fill={AREA_FILL[props.type]}
        onClick={(event) => {
            props.selectArea();
            event.stopPropagation();
        }}
    />
);

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    type: getObjectType(state, ownProps.name) as RectangleObjectType,
    width: getWidth(state, ownProps.name),
    height: getHeight(state, ownProps.name),
});

const mapDispatchToProps = (
    dispatch: Dispatch,
    ownProps: OwnProps
): DispatchProps => ({
    selectArea: () => {
        dispatch(drawingActions.setSelectedObjects([ownProps.name]));
    },
});

export const RectangleAreaSymbol = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedRectangleAreaSymbol);
