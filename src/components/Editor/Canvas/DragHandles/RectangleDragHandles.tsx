import { Dispatch } from "@reduxjs/toolkit";
import * as React from "react";
import { connect } from "react-redux";
import { drawingActions } from "../../../../state/drawingSlice/drawingSlice";
import {
    getObjectLocation,
    getObjectType,
    getZoomLevel,
    getPanPosition,
    getHeight,
    getWidth,
    getOrientation,
} from "../../../../state/drawingSlice/selectors";
import { Coordinates, ObjectType } from "../../../../state/drawingSlice/types";
import { getRectangleCornerPoints } from "../../../../state/drawingSlice/utils";
import { StoreState } from "../../../../state/store";
import { degToRad } from "../../../../utils/MathUtils";
import { getCanvasContainerLocation } from "../utils";
import { SetSnapFunction, snapLocationToLine } from "./utils";

interface DispatchProps {
    setDimensions: (dimensions: { width?: number; height?: number }) => void;
    moveArea: (location: Coordinates) => void;
    setOrientation: (orientation: number) => void;
    setSnapLines: SetSnapFunction;
    resetSnapLines: () => void;
}

interface StateProps {
    height: number;
    width: number;
    orientation: number;
    location: Coordinates;
    type: ObjectType;
    zoomLevel: number;
    panPosition: Coordinates;
}

type OwnProps = { name: string };

type RectangleDragHandlesProps = StateProps & DispatchProps & OwnProps;

const enum DragPointLocation {
    Top,
    Bottom,
    Right,
    Left,
}

interface DragHandlePoint {
    x: number;
    y: number;
    location: DragPointLocation;
}

const UnconnectedRectangleDragHandles: React.FC<RectangleDragHandlesProps> = (
    props
) => {
    const handlePoints = getDragHandlePoints(props);
    return (
        <g className="drag-handle__overlay">
            <rect
                className="drag-handle__area"
                height={props.height}
                width={props.width}
                onPointerDown={makeHadleAreaOnPointerDownFunction({
                    props,
                })}
            />
            {handlePoints.map((point, index) => (
                <circle
                    className="drag-handle__point"
                    key={index}
                    cx={point.x}
                    cy={point.y}
                    r="5"
                    onPointerDown={makeHandlePointOnPointerDownFunction({
                        point,
                        props,
                    })}
                />
            ))}
        </g>
    );
};

const getDragHandlePoints = (
    props: RectangleDragHandlesProps
): DragHandlePoint[] => {
    if (props.type === ObjectType.Plant) {
        return [
            { x: props.width / 2, y: 0, location: DragPointLocation.Top },
            {
                x: props.width / 2,
                y: props.height,
                location: DragPointLocation.Bottom,
            },
        ];
    } else {
        return [
            {
                x: 0,
                y: props.height / 2,
                location: DragPointLocation.Left,
            },
            {
                x: props.width,
                y: props.height / 2,
                location: DragPointLocation.Right,
            },
            { x: props.width / 2, y: 0, location: DragPointLocation.Top },
            {
                x: props.width / 2,
                y: props.height,
                location: DragPointLocation.Bottom,
            },
        ];
    }
};

interface HandleAreaMouseDownOptions {
    props: RectangleDragHandlesProps;
}

const makeHadleAreaOnPointerDownFunction = ({
    props,
}: HandleAreaMouseDownOptions) => (e: React.PointerEvent<SVGRectElement>) => {
    if (e.pointerType !== "mouse" || e.button === 0) {
        const canvasLocation = getCanvasContainerLocation();
        const dragOffset = {
            x:
                (e.pageX - props.panPosition.x - canvasLocation.x) /
                    props.zoomLevel -
                props.location.x,
            y:
                (e.pageY - props.panPosition.y - canvasLocation.y) /
                    props.zoomLevel -
                props.location.y,
        };
        const onPointerMove = (event: MouseEvent) => {
            const location = {
                x:
                    (event.pageX - props.panPosition.x - canvasLocation.x) /
                        props.zoomLevel -
                    dragOffset.x,
                y:
                    (event.pageY - props.panPosition.y - canvasLocation.y) /
                        props.zoomLevel -
                    dragOffset.y,
            };
            props.moveArea(
                snapLocationToLine({
                    snapLocations: getRectangleCornerPoints(props.name, {
                        location: location,
                        width: props.width,
                        height: props.height,
                        orientation: props.orientation,
                    }),
                    objectLocation: location,
                    setSnapLines: props.setSnapLines,
                })
            );
        };
        const onPointerUp = (event: MouseEvent) => {
            document.removeEventListener("pointermove", onPointerMove);
            document.removeEventListener("pointerup", onPointerUp);
            props.resetSnapLines();
            event.stopPropagation();
        };
        document.addEventListener("pointermove", onPointerMove);
        document.addEventListener("pointerup", onPointerUp);
        e.stopPropagation();
    }
};

interface DragPointMouseDownFunction extends HandleAreaMouseDownOptions {
    point: DragHandlePoint;
}

const makeHandlePointOnPointerDownFunction = ({
    point,
    props,
}: DragPointMouseDownFunction) => (e: React.PointerEvent<SVGCircleElement>) => {
    const canvasLocation = getCanvasContainerLocation();
    const onPointerMove = (event: PointerEvent) => {
        const x =
            (event.pageX - props.panPosition.x - canvasLocation.x) /
                props.zoomLevel -
            props.location.x;
        const y =
            (event.pageY - props.panPosition.y - canvasLocation.y) /
                props.zoomLevel -
            props.location.y;
        if (point.location === DragPointLocation.Bottom) {
            props.setDimensions({
                height: getProjection(x, y, props.orientation, "y"),
            });
        } else if (point.location === DragPointLocation.Right) {
            props.setDimensions({
                width: getProjection(x, y, props.orientation, "x"),
            });
        } else if (point.location === DragPointLocation.Top) {
            const deltaHeight =
                -1 * getProjection(x, y, props.orientation, "y");
            const newHeight = props.height + deltaHeight;
            props.moveArea({
                x:
                    Number(props.location.x) +
                    Math.sin(degToRad(props.orientation)) * deltaHeight,
                y:
                    Number(props.location.y) -
                    Math.cos(degToRad(props.orientation)) * deltaHeight,
            });
            props.setDimensions({
                height: newHeight,
            });
        } else if (point.location === DragPointLocation.Left) {
            const deltaWidth = -1 * getProjection(x, y, props.orientation, "x");
            const newWidth = props.width + deltaWidth;
            props.moveArea({
                x:
                    Number(props.location.x) -
                    Math.cos(degToRad(props.orientation)) * deltaWidth,
                y:
                    Number(props.location.y) -
                    Math.sin(degToRad(props.orientation)) * deltaWidth,
            });
            props.setDimensions({
                width: newWidth,
            });
        }
    };
    const onPointerUp = (event: PointerEvent) => {
        document.removeEventListener("pointermove", onPointerMove);
        document.removeEventListener("pointerup", onPointerUp);
        event.stopPropagation();
    };
    document.addEventListener("pointermove", onPointerMove);
    document.addEventListener("pointerup", onPointerUp);
    e.stopPropagation();
};

const getProjection = (
    x: number,
    y: number,
    orientation: number,
    projectTo: "x" | "y"
) => {
    const radial = Math.sqrt(x ** 2 + y ** 2);
    const angle =
        x >= 0
            ? (Math.atan(y / x) * 180) / Math.PI
            : (Math.atan(y / x) * 180) / Math.PI + 180;
    let deltaAngle = angle - orientation;
    if (projectTo === "y") {
        deltaAngle = deltaAngle - 90;
    }
    return Math.cos(deltaAngle * (Math.PI / 180)) * radial;
};

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    height: getHeight(state, ownProps.name),
    width: getWidth(state, ownProps.name),
    orientation: getOrientation(state, ownProps.name),
    location: getObjectLocation(state, ownProps.name),
    type: getObjectType(state, ownProps.name),
    zoomLevel: getZoomLevel(state),
    panPosition: getPanPosition(state),
});

const mapDispatchToProps = (
    dispatch: Dispatch,
    ownProps: OwnProps
): DispatchProps => ({
    setDimensions: (dimensions) => {
        dispatch(
            drawingActions.setRectangleDimensions({
                name: ownProps.name,
                ...dimensions,
            })
        );
    },
    moveArea: (location) => {
        dispatch(drawingActions.moveObject({ name: ownProps.name, location }));
    },
    setOrientation: (orientation) =>
        dispatch(
            drawingActions.setOrientation({ name: ownProps.name, orientation })
        ),
    setSnapLines: (x, y) => {
        dispatch(drawingActions.setSnapLines({ x, y }));
    },
    resetSnapLines: () => {
        dispatch(drawingActions.resetSnapLines());
    },
});

export const RectangleDragHandles = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedRectangleDragHandles);
