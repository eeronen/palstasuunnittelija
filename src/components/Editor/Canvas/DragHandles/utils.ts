import {
    getSnapPoints,
    SnapPoint,
} from "../../../../state/drawingSlice/selectors";
import { Coordinates } from "../../../../state/drawingSlice/types";
import { getRectangleCornerPoints } from "../../../../state/drawingSlice/utils";
import { store } from "../../../../state/store";

interface SnapOptions {
    snapLocations: SnapPoint[];
    objectLocation: Coordinates;
    setSnapLines?: SetSnapFunction;
}

export type SetSnapFunction = (x: number[], y: number[]) => void;

export const snapLocationToLine = ({
    objectLocation,
    snapLocations,
    setSnapLines,
}: SnapOptions) => {
    const diagramSnapPoints = getSnapPoints(
        store.getState()
    ).filter((snapPoint) =>
        snapLocations.every(
            (location) =>
                location.objectName !== snapPoint.objectName ||
                (location.pointIndex !== undefined &&
                    location.pointIndex !== snapPoint.pointIndex)
        )
    );

    const snappedLocation = { ...objectLocation };
    const xSnapLines = new Set<number>();
    const ySnapLines = new Set<number>();
    diagramSnapPoints.forEach((diagramPoint) => {
        if (xSnapLines.size === 0) {
            const closeX = snapLocations.find(
                (location) => Math.abs(location.x - diagramPoint.x) < 5
            );

            if (closeX !== undefined) {
                const deltaXToSnap = diagramPoint.x - closeX.x;
                snappedLocation.x = snappedLocation.x + deltaXToSnap;
                xSnapLines.add(diagramPoint.x);
            }
        }
        if (ySnapLines.size === 0) {
            const closeY = snapLocations.find(
                (location) => Math.abs(location.y - diagramPoint.y) < 5
            );

            if (closeY !== undefined) {
                const deltaYToSnap = diagramPoint.y - closeY.y;
                snappedLocation.y = snappedLocation.y + deltaYToSnap;
                ySnapLines.add(diagramPoint.y);
            }
        }
        if (ySnapLines.size === 0 && xSnapLines.size === 0) {
            setSnapLines([...xSnapLines], [...ySnapLines]);
            return snappedLocation;
        }
    });
    setSnapLines([...xSnapLines], [...ySnapLines]);
    return snappedLocation;
};

interface SnapHeightOptions {
    objectLocation: Coordinates;
    height: number;
    setSnapLines: SetSnapFunction;
    objectName: string;
    width: number;
    orientation: number;
}

export const snapHeightToLine = ({
    objectName,
    objectLocation,
    setSnapLines,
    height,
    width,
    orientation,
}: SnapHeightOptions) => {
    const diagramSnapPoints = getSnapPoints(store.getState()).filter(
        (snapPoint) => snapPoint.objectName !== objectName
    );
    const snapLocations = getRectangleCornerPoints(objectName, {
        location: objectLocation,
        height,
        width,
        orientation,
    });
    let snappedHeight = height;
    const xSnapLines = new Set<number>();
    const ySnapLines = new Set<number>();
    diagramSnapPoints.forEach((diagramPoint) => {
        if (xSnapLines.size === 0) {
            const closeX = snapLocations.find(
                (location) => Math.abs(location.x - diagramPoint.x) < 5
            );

            if (closeX !== undefined) {
                const deltaXToSnap = diagramPoint.x - closeX.x;
                snappedHeight = height + Math.cos(orientation) * deltaXToSnap;
                xSnapLines.add(diagramPoint.x);
            }
        }
        if (ySnapLines.size === 0) {
            const closeY = snapLocations.find(
                (location) => Math.abs(location.y - diagramPoint.y) < 5
            );

            if (closeY !== undefined) {
                const deltaYToSnap = diagramPoint.y - closeY.y;
                snappedHeight = height + deltaYToSnap;
                ySnapLines.add(diagramPoint.y);
            }
        }
        if (ySnapLines.size === 0 && xSnapLines.size === 0) {
            setSnapLines([...xSnapLines], [...ySnapLines]);
            return snappedHeight;
        }
    });
    setSnapLines([...xSnapLines], [...ySnapLines]);
    return snappedHeight;
};
