import * as React from "react";
import { connect } from "react-redux";
import { getActiveSnapLines } from "../../../../state/drawingSlice/selectors";
import { StoreState } from "../../../../state/store";

interface StateProps {
    xSnapLines: number[];
    ySnapLines: number[];
}

type SnapLineProps = StateProps;

const UnconnectedSnapLines: React.FC<SnapLineProps> = (props) => {
    return (
        <>
            {props.xSnapLines.map((lineLocation, index) => (
                <line
                    key={`x-${index}`}
                    className="drawing-canvas__snap-line"
                    x1={lineLocation}
                    x2={lineLocation}
                    y1="-100%"
                    y2="100%"
                />
            ))}

            {props.ySnapLines.map((lineLocation, index) => (
                <line
                    key={`y-${index}`}
                    className="drawing-canvas__snap-line"
                    x1="-100%"
                    x2="100%"
                    y1={lineLocation}
                    y2={lineLocation}
                />
            ))}
        </>
    );
};

const mapStateToProps = (state: StoreState): StateProps => {
    const snapLines = getActiveSnapLines(state);
    return {
        xSnapLines: snapLines.x,
        ySnapLines: snapLines.y,
    };
};

export const SnapLines = connect(mapStateToProps)(UnconnectedSnapLines);
