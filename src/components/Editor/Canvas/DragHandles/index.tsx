import * as React from "react";
import { connect } from "react-redux";
import { getObjectType } from "../../../../state/drawingSlice/selectors";
import {
    ObjectType,
    isPolygonObject,
} from "../../../../state/drawingSlice/types";
import { StoreState } from "../../../../state/store";
import { PolygonDragHandles } from "./PolygonDragHandles";
import { RectangleDragHandles } from "./RectangleDragHandles";

import "./DragHandles.scss";
interface OwnProps {
    name: string;
}

interface StateProps {
    type: ObjectType;
}

type DragHandleProps = OwnProps & StateProps;

const UnconnectedDragHandles: React.FC<DragHandleProps> = (props) => {
    if (isPolygonObject(props.type)) {
        return <PolygonDragHandles name={props.name} />;
    } else {
        return <RectangleDragHandles name={props.name} />;
    }
};

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    type: getObjectType(state, ownProps.name),
});

export const DragHandles = connect(mapStateToProps)(UnconnectedDragHandles);
