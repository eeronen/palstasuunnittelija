import { Dispatch } from "@reduxjs/toolkit";
import * as React from "react";
import { connect } from "react-redux";
import { drawingActions } from "../../../../state/drawingSlice/drawingSlice";
import {
    getObjectPoints,
    getObjectLocation,
    getObjectType,
    getZoomLevel,
    getPanPosition,
} from "../../../../state/drawingSlice/selectors";
import { Coordinates, ObjectType } from "../../../../state/drawingSlice/types";
import { StoreState } from "../../../../state/store";
import { getPolygonPoints, getCanvasContainerLocation } from "../utils";
import { SetSnapFunction, snapLocationToLine } from "./utils";

interface DispatchProps {
    moveAreaPoint: (pointIndex: number, location: Coordinates) => void;
    adjustAreaCoordinates: (name: string) => void;
    moveArea: (location: Coordinates) => void;
    addPoint: (location: Coordinates, index: number) => void;
    setSnapLines: SetSnapFunction;
    resetSnapLines: () => void;
}

interface StateProps {
    points: Coordinates[];
    location: Coordinates;
    type: ObjectType;
    zoomLevel: number;
    panPosition: Coordinates;
}

type OwnProps = { name: string };

type DragHandleProps = StateProps & DispatchProps & OwnProps;

const UnconnectedPolygonDragHandles: React.FC<DragHandleProps> = (props) => {
    return (
        <g className="drag-handle__overlay">
            <polygon
                points={getPolygonPoints(props.points)}
                className="drag-handle__area"
                onPointerDown={makeAreaMovePointerDownHandler({
                    props,
                })}
            />
            {props.points.map((point, index) => (
                <circle
                    className="drag-handle__point"
                    key={index}
                    cx={point.x}
                    cy={point.y}
                    r="5"
                    onPointerDown={makePointMovePointerDownHandler({
                        props,
                        point,
                        index,
                    })}
                />
            ))}
            {getMiddlePoints(props.points).map((point, index) => (
                <circle
                    className="drag-handle__middle-point"
                    key={index}
                    cx={point.x}
                    cy={point.y}
                    r="5"
                    onClick={() => props.addPoint(point, index + 1)}
                />
            ))}
        </g>
    );
};

const getMiddlePoints = (points: Coordinates[]) =>
    points.map((point, index) => {
        let point1 = point;
        let point2 = index < points.length - 1 ? points[index + 1] : points[0];
        return {
            x: (point1.x + point2.x) / 2,
            y: (point1.y + point2.y) / 2,
        };
    });

interface HandleAreaMouseDownOptions {
    props: DragHandleProps;
}

const makeAreaMovePointerDownHandler = ({
    props,
}: HandleAreaMouseDownOptions): React.PointerEventHandler => (e) => {
    if (e.button === 0 || e.pointerType !== "mouse") {
        const canvasLocation = getCanvasContainerLocation();
        const dragOffset = {
            x:
                (e.pageX - props.panPosition.x - canvasLocation.x) /
                    props.zoomLevel -
                props.location.x,
            y:
                (e.pageY - props.panPosition.y - canvasLocation.y) /
                    props.zoomLevel -
                props.location.y,
        };
        const onPointerMove = (event: PointerEvent) => {
            const location = {
                x:
                    (event.pageX - props.panPosition.x - canvasLocation.x) /
                        props.zoomLevel -
                    dragOffset.x,
                y:
                    (event.pageY - props.panPosition.y - canvasLocation.y) /
                        props.zoomLevel -
                    dragOffset.y,
            };
            props.moveArea(
                snapLocationToLine({
                    snapLocations: props.points.map((point, pointIndex) => ({
                        x: point.x + props.location.x,
                        y: point.y + props.location.y,
                        objectName: props.name,
                        pointIndex,
                    })),
                    objectLocation: location,
                    setSnapLines: props.setSnapLines,
                })
            );
        };
        const onPointerUp = (event: PointerEvent) => {
            document.removeEventListener("pointermove", onPointerMove);
            document.removeEventListener("pointerup", onPointerUp);
            props.resetSnapLines();
            event.stopPropagation();
        };
        document.addEventListener("pointermove", onPointerMove);
        document.addEventListener("pointerup", onPointerUp);
        e.stopPropagation();
    }
};

interface HandlePointMouseDownOptions extends HandleAreaMouseDownOptions {
    point: Coordinates;
    index: number;
}

const makePointMovePointerDownHandler = ({
    props,
    point,
    index,
}: HandlePointMouseDownOptions): React.PointerEventHandler => (e) => {
    const canvasLocation = getCanvasContainerLocation();
    const dragOffset = {
        x:
            (e.pageX - props.panPosition.x - canvasLocation.x) /
                props.zoomLevel -
            point.x,
        y:
            (e.pageY - props.panPosition.y - canvasLocation.y) /
                props.zoomLevel -
            point.y,
    };
    const onPointerMove = (event: PointerEvent) => {
        const location = {
            x:
                (event.pageX - props.panPosition.x - canvasLocation.x) /
                    props.zoomLevel -
                dragOffset.x +
                props.location.x,
            y:
                (event.pageY - props.panPosition.y - canvasLocation.y) /
                    props.zoomLevel -
                dragOffset.y +
                props.location.y,
        };
        const snappedAbsoluteLocation = snapLocationToLine({
            snapLocations: [
                {
                    x: location.x,
                    y: location.y,
                    pointIndex: index,
                    objectName: props.name,
                },
            ],
            objectLocation: location,
            setSnapLines: props.setSnapLines,
        });
        props.moveAreaPoint(index, {
            x: snappedAbsoluteLocation.x - props.location.x,
            y: snappedAbsoluteLocation.y - props.location.y,
        });
    };
    const onPointerUp = (event: PointerEvent) => {
        document.removeEventListener("pointermove", onPointerMove);
        document.removeEventListener("pointerup", onPointerUp);
        props.adjustAreaCoordinates(props.name);
        props.resetSnapLines();
        event.stopPropagation();
    };
    document.addEventListener("pointermove", onPointerMove);
    document.addEventListener("pointerup", onPointerUp);
    e.stopPropagation();
};

const mapStateToProps = (
    state: StoreState,
    ownProps: OwnProps
): StateProps => ({
    points: getObjectPoints(state, ownProps.name),
    location: getObjectLocation(state, ownProps.name),
    type: getObjectType(state, ownProps.name),
    zoomLevel: getZoomLevel(state),
    panPosition: getPanPosition(state),
});

const mapDispatchToProps = (
    dispatch: Dispatch,
    ownProps: OwnProps
): DispatchProps => ({
    moveAreaPoint: (pointIndex, location) => {
        dispatch(
            drawingActions.moveObjectPoint({
                name: ownProps.name,
                pointIndex,
                location,
            })
        );
    },
    adjustAreaCoordinates: (name) => {
        dispatch(drawingActions.adjustCoordinates(name));
    },
    moveArea: (location) => {
        dispatch(drawingActions.moveObject({ name: ownProps.name, location }));
    },
    addPoint: (location, index) => {
        dispatch(
            drawingActions.addPointToRectangle({
                name: ownProps.name,
                location,
                index,
            })
        );
    },
    setSnapLines: (x, y) => {
        dispatch(drawingActions.setSnapLines({ x, y }));
    },
    resetSnapLines: () => {
        dispatch(drawingActions.resetSnapLines());
    },
});

export const PolygonDragHandles = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedPolygonDragHandles);
