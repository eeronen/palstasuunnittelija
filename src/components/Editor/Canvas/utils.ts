import { ObjectType, Coordinates } from "../../../state/drawingSlice/types";

export const getPolygonPoints = (points: Coordinates[]) =>
    points.reduce(
        (pointsString, point) =>
            (pointsString = `${pointsString} ${point.x},${point.y}`),
        ""
    );
export const AREA_FILL: { [key in ObjectType]: string } = {
    [ObjectType.Base]: "#aaaaaa",
    [ObjectType.Lavakaulus]: "#7f6c57",
    [ObjectType.Other]: "#cecece",
    [ObjectType.Path]: "#231f1a",
    [ObjectType.Plant]: "#7c623b",
};

export const getCanvasContainerLocation = () => {
    const rect = document
        .querySelector(".drawing-canvas__div")
        .getBoundingClientRect();
    return {
        x: rect.left,
        y: rect.top,
    };
};
