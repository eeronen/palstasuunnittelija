import * as React from "react";
import { connect } from "react-redux";
import {
    getObjectLocation,
    isSelected,
    getOrientation,
    getObjectType,
    getWidth,
    getHeight,
} from "../../../state/drawingSlice/selectors";
import {
    Coordinates,
    isRectangleObject,
} from "../../../state/drawingSlice/types";
import { StoreState } from "../../../state/store";
import { AreaSymbol } from "./AreaSymbols";
import { DimensionMarker } from "./DimensionMarkers";
import { DragHandles } from "./DragHandles";

interface StateProps {
    isSelected: boolean;
    location: Coordinates;
    orientation: number;
    rotateOrigin: Coordinates;
}

interface OwnProps {
    name: string;
}

type AreaProps = StateProps & OwnProps;

export const UnconnectedArea: React.FC<AreaProps> = (props) => {
    return (
        <g
            className="drawing-area__container"
            transform={`translate(${props.location.x},${props.location.y}) rotate(${props.orientation})`}
        >
            <AreaSymbol name={props.name} />
            {props.isSelected && (
                <>
                    <DragHandles name={props.name} />
                    <DimensionMarker name={props.name} />
                </>
            )}
        </g>
    );
};

const mapStateToProps = (state: StoreState, ownProps: OwnProps): StateProps => {
    const { name } = ownProps;
    let rotateOrigin = { x: 0, y: 0 };
    if (isRectangleObject(getObjectType(state, name))) {
        rotateOrigin.x = getWidth(state, name) / 2;
        rotateOrigin.y = getHeight(state, name) / 2;
    }
    return {
        isSelected: isSelected(state, name),
        location: getObjectLocation(state, name),
        orientation: getOrientation(state, name),
        rotateOrigin,
    };
};

export const Area = connect(mapStateToProps)(UnconnectedArea);
