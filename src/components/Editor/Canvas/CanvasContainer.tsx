import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { drawingActions } from "../../../state/drawingSlice/drawingSlice";
import { getPanPosition } from "../../../state/drawingSlice/selectors";
import { Coordinates } from "../../../state/drawingSlice/types";
import { StoreState } from "../../../state/store";
import { PointerEventHelper } from "../../../utils/PointerHelper";

interface StateProps {
    panPosition: Coordinates;
}

interface DispatchProps {
    clearSelection: () => void;
    zoomIn: () => void;
    zoomOut: () => void;
    setPanPosition: (position: Coordinates) => void;
}

type CanvasProps = StateProps & DispatchProps;

export const UnconnectedCanvasContainer: React.FC<CanvasProps> = (props) => {
    return (
        <div
            className="drawing-canvas__div"
            style={{ height: "100%", width: "100%", overflow: "hidden" }}
            onWheel={(event) => {
                if (event.deltaY > 0) {
                    props.zoomOut();
                } else {
                    props.zoomIn();
                }
            }}
            onPointerDown={(event) => {
                PointerEventHelper.eventStarted(event);
                if (event.pointerType !== "mouse" || event.button === 1) {
                    const dragOffset = {
                        x: event.pageX - props.panPosition.x,
                        y: event.pageY - props.panPosition.y,
                    };
                    const onPointerMove = (moveEvent: PointerEvent) => {
                        PointerEventHelper.eventStarted(moveEvent);
                        if (PointerEventHelper.isZoomEventActive()) {
                            const zoomDelta = PointerEventHelper.getZoomDelta();
                            if (zoomDelta > 0) {
                                props.zoomIn();
                            } else {
                                props.zoomOut();
                            }
                        } else {
                            props.setPanPosition({
                                x: moveEvent.pageX - dragOffset.x,
                                y: moveEvent.pageY - dragOffset.y,
                            });
                        }
                    };
                    const onPointerUp = (upEvent: PointerEvent) => {
                        PointerEventHelper.eventEnded(upEvent);
                        document.removeEventListener(
                            "pointermove",
                            onPointerMove
                        );
                        document.removeEventListener("pointerup", onPointerUp);
                    };
                    document.addEventListener("pointermove", onPointerMove);
                    document.addEventListener("pointerup", onPointerUp);
                    event.stopPropagation();
                }
            }}
            onClick={(event) => {
                if (
                    (event.target as HTMLElement).closest(
                        ".drawing-symbol__area"
                    ) === null &&
                    (event.target as HTMLElement).closest(
                        ".drag-handle__area"
                    ) === null
                ) {
                    props.clearSelection();
                }
            }}
        >
            {props.children}
        </div>
    );
};

const mapStateToProps = (state: StoreState): StateProps => ({
    panPosition: getPanPosition(state),
});

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => ({
    clearSelection: () => {
        dispatch(drawingActions.setSelectedObjects([]));
    },
    zoomIn: () => dispatch(drawingActions.zoomIn()),
    zoomOut: () => dispatch(drawingActions.zoomOut()),
    setPanPosition: (position) =>
        dispatch(drawingActions.setPanPosition(position)),
});

export const CanvasContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(UnconnectedCanvasContainer);
