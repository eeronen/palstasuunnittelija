import * as React from "react";
import { MONTHS } from "./utils";

export const CalendarIndex: React.FC<{}> = () => (
    <div className="calendar__index">
        <div className="calendar__index-text calendar__name">Kasvi</div>
        {MONTHS.map((month) => (
            <div key={month} className="calendar__index-text calendar__cell">
                {month}
            </div>
        ))}
    </div>
);
