import * as React from "react";
import { monthToLeftPosition } from "./utils";

const DAYS_IN_MONTH = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

export const DateLine: React.FC<{}> = () => {
    const date = new Date();
    const month =
        date.getMonth() + 1 + date.getDate() / DAYS_IN_MONTH[date.getMonth()];
    return (
        <div
            className="calendar__date-line"
            style={{ left: monthToLeftPosition(month * 10) }}
        ></div>
    );
};
