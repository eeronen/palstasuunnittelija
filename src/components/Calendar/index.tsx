import * as React from "react";
import { connect } from "react-redux";
import { getSeeds } from "../../state/SeedsSlice/selectors";
import { SeedData } from "../../state/SeedsSlice/types";
import { StoreState } from "../../state/store";

import "./Calendar.scss";
import { DateLine } from "./DateLine";
import { CalendarIndex } from "./Header";
import { PlantRow } from "./PlantRow";

interface StateProps {
    seeds: [string, SeedData][];
}

export const UnconnectedCalendar: React.FC<StateProps> = (props) => {
    return (
        <div className="calendar__container">
            <div className="calendar__table">
                <DateLine />
                <CalendarIndex />
                {props.seeds.map(([seedName, data], index) => (
                    <PlantRow
                        key={index}
                        index={index}
                        seedName={seedName}
                        preGrow={data.preGrow}
                        planting={data.planting}
                        harvest={data.harvest}
                    />
                ))}
            </div>
        </div>
    );
};

const mapStateToProps = (state: StoreState): StateProps => ({
    seeds: Object.entries(getSeeds(state)),
});

export const Calendar = connect(mapStateToProps)(UnconnectedCalendar);
