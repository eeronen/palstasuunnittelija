export const MONTHS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

export const monthToLeftPosition = (month: number) =>
    `${(month - 10) * 0.75 + 10}%`;
export const monthsToWidth = ([start, end]: [number, number]) =>
    `${(end - start) * 0.75}%`;
