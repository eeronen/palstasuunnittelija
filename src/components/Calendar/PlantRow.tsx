import * as React from "react";
import { MONTHS, monthsToWidth, monthToLeftPosition } from "./utils";

interface PlantRowProps {
    index: number;
    seedName: string;
    preGrow?: [number, number];
    planting?: [number, number];
    harvest?: [number, number];
}

export const PlantRow: React.FC<PlantRowProps> = (props) => {
    return (
        <div className="calendar__row" tabIndex={props.index}>
            <div className="calendar__name">{props.seedName}</div>
            {MONTHS.map((month) => (
                <div className="calendar__cell" key={month}></div>
            ))}
            {props.preGrow && (
                <div
                    className="calendar__pregrow"
                    style={{
                        left: monthToLeftPosition(props.preGrow[0]),
                        width: monthsToWidth(props.preGrow),
                    }}
                />
            )}
            {props.planting && (
                <div
                    className="calendar__planting"
                    style={{
                        left: monthToLeftPosition(props.planting[0]),
                        width: monthsToWidth(props.planting),
                    }}
                />
            )}
            {props.harvest && (
                <div
                    className="calendar__harvest"
                    style={{
                        left: monthToLeftPosition(props.harvest[0]),
                        width: monthsToWidth(props.harvest),
                    }}
                />
            )}
        </div>
    );
};
