import * as React from "react";
import { Link } from "react-router-dom";
import { Routes } from "../../constants";

export const HomePage: React.FC<{}> = (props) => {
    return (
        <div className="home-page__container">
            <Link to={Routes.Editor}>Editori</Link>
            <Link to={Routes.Calendar}>Kalenteri</Link>
        </div>
    );
};
