import * as React from "react";
import { Link } from "react-router-dom";
import { CalendarIcon, DrawingEditorIcon, SeedsIcon } from "./Icons";
import { Routes } from "../constants";
import "./SideMenu.scss";
import { store } from "../state/store";
import { getDrawing } from "../services/drawings";
import { getSeeds } from "../services/seeds";
import { drawingActions } from "../state/drawingSlice/drawingSlice";
import { seedActions } from "../state/SeedsSlice";

export const SideMenu: React.FC<{}> = (props) => {
    React.useEffect(() => {
        (async () => {
            const drawing = await getDrawing();
            store.dispatch(drawingActions.initializeDrawing(drawing));
            const seeds = await getSeeds();
            store.dispatch(seedActions.initializeSeedData(seeds));
        })();
    }, []);
    return (
        <div className="side-menu">
            <Link className="side-menu__item" to={Routes.Editor}>
                <DrawingEditorIcon />
            </Link>
            <Link to={Routes.Calendar} className="side-menu__item">
                <CalendarIcon />
            </Link>
            <Link to={Routes.SeedList} className="side-menu__item">
                <SeedsIcon />
            </Link>
        </div>
    );
};
