import * as React from "react";

export const SeedsIcon: React.FC<{}> = () => (
    <svg
        version="1.1"
        baseProfile="full"
        width="100%"
        height="100%"
        viewBox="0 0 100 100"
        xmlns="http://www.w3.org/2000/svg"
        className="icon"
    >
        <path
            className="fill-secondary"
            strokeWidth="5"
            d="M50,100 L50,60 Q90,60 90,20 Q50,20 50,60"
        />
        <path
            className="fill-secondary"
            strokeWidth="5"
            d="M50,100 L50,50 Q50,5 5,5 Q5,50 50,50"
        />
        <line x1="5" y1="96" x2="96" y2="96" strokeWidth="8" />
    </svg>
);
