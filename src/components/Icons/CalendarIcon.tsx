import * as React from "react";

export const CalendarIcon: React.FC<{}> = (props) => (
    <svg
        version="1.1"
        baseProfile="full"
        width="100%"
        height="100%"
        viewBox="0 0 100 100"
        xmlns="http://www.w3.org/2000/svg"
        className="icon"
    >
        <rect
            className="fill-secondary"
            strokeWidth="10"
            x="5"
            y="5"
            rx="15"
            ry="15"
            width="90"
            height="90"
        />
        <line x1="10" y1="20" x2="65" y2="20" strokeWidth="10" />
        <line x1="25" y1="35" x2="90" y2="35" strokeWidth="10" />
        <line x1="15" y1="50" x2="80" y2="50" strokeWidth="10" />
        <line x1="30" y1="65" x2="85" y2="65" strokeWidth="10" />
        <line x1="20" y1="80" x2="90" y2="80" strokeWidth="10" />
    </svg>
);
