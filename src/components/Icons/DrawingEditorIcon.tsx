import * as React from "react";

export const DrawingEditorIcon: React.FC<{}> = () => (
    <svg
        version="1.1"
        baseProfile="full"
        width="100%"
        height="100%"
        viewBox="0 0 100 100"
        xmlns="http://www.w3.org/2000/svg"
        className="icon"
    >
        <rect
            className="fill-secondary"
            strokeWidth="5"
            x="5"
            y="5"
            width="60"
            height="60"
        />
        <rect
            className="fill-secondary"
            strokeWidth="5"
            x="30"
            y="30"
            width="60"
            height="60"
        />
        <circle r="7.5" cx="30" cy="30" />
        <circle r="7.5" cx="90" cy="30" />
        <circle r="7.5" cx="30" cy="90" />
        <circle r="7.5" cx="90" cy="90" />
    </svg>
);
