import * as React from "react";

export const ArrowIcon: React.FC<{}> = () => (
    <svg
        version="1.1"
        baseProfile="full"
        width="100%"
        height="100%"
        viewBox="0 0 100 100"
        xmlns="http://www.w3.org/2000/svg"
        className="icon"
    >
        <path
            fill="transparent"
            stroke-width="10"
            d="M15,5 L85,50 L15,95"
            stroke-linecap="round"
        />
    </svg>
);
