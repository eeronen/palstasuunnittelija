import { DrawingObjectWithNames } from "../state/drawingSlice/types";
import { DrawingObjectTransferFormat } from "../types";
import { environment } from "../environment";

export function getDrawing() {
    return new Promise((resolve, reject) => {
        const xmlhttp = new XMLHttpRequest();
        xmlhttp.onload = () => {
            resolve(parseDrawingObjectResponse(xmlhttp.responseText));
        };
        xmlhttp.onerror = () => reject(xmlhttp.statusText);
        xmlhttp.open("GET", environment.apiUrl + "/get_drawing.php", true);
        xmlhttp.send();
    }) as Promise<DrawingObjectWithNames[]>;
}

export function parseDrawingObjectResponse(
    data: string
): DrawingObjectWithNames[] {
    const rawResponse = JSON.parse(data) as DrawingObjectTransferFormat[];
    return rawResponse.map(
        (
            drawingObject: DrawingObjectTransferFormat
        ): DrawingObjectWithNames => {
            convertStringsToNumbers(drawingObject);
            const { x, y, shape_data, ...objectData } = drawingObject;
            return {
                ...objectData,
                location: { x, y },
                ...shape_data,
            };
        }
    );
}

export function commitDrawing(data: DrawingObjectWithNames[]) {
    return new Promise((resolve, reject) => {
        const xmlhttp = new XMLHttpRequest();
        xmlhttp.onload = () => {
            if (xmlhttp.status === 200) {
                resolve(true);
            } else {
                reject(false);
            }
        };
        xmlhttp.onerror = () => reject(xmlhttp.statusText);
        xmlhttp.open("POST", environment.apiUrl + "/post_drawing.php", true);
        xmlhttp.setRequestHeader(
            "Content-Type",
            "application/x-www-form-urlencoded"
        );
        xmlhttp.send(
            `json_data=${encodeURIComponent(serializeDrawingObject(data))}`
        );
        resolve([]);
    });
}

function serializeDrawingObject(data: DrawingObjectWithNames[]): string {
    const formattedData: DrawingObjectTransferFormat[] = data.map((object) => {
        convertStringsToNumbers(object);
        const {
            orientation,
            name,
            location,
            parent,
            type,
            id,
            ...shapeData
        } = object;
        return {
            orientation,
            name,
            parent,
            type,
            id,
            shape_data: shapeData,
            x: location.x,
            y: location.y,
        };
    });
    return JSON.stringify(formattedData);
}

function convertStringsToNumbers(object: any) {
    Object.keys(object).forEach((key) => {
        const maybeNumber = Number(object[key]);
        if (object[key] !== null && !isNaN(maybeNumber)) {
            object[key] = maybeNumber;
        }
    });
}
