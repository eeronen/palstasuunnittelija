import { environment } from "../environment";
import {
    SeedDataWithName,
    SeedTransferFormat,
} from "../state/SeedsSlice/types";

export function getSeeds() {
    return new Promise((resolve, reject) => {
        const xmlhttp = new XMLHttpRequest();
        xmlhttp.onload = () => {
            resolve(parseSeedsResponse(xmlhttp.responseText));
        };
        xmlhttp.onerror = () => reject(xmlhttp.statusText);
        xmlhttp.open("GET", environment.apiUrl + "/get_seeds.php", true);
        xmlhttp.send();
    }) as Promise<SeedDataWithName[]>;
}

export function parseSeedsResponse(data: string): SeedDataWithName[] {
    const rawResponse = JSON.parse(data) as SeedTransferFormat[];
    return rawResponse.map(
        (seed: SeedTransferFormat): SeedDataWithName => {
            const {
                name,
                id,
                pre_grow_start,
                pre_grow_end,
                planting_start,
                planting_end,
                harvest_start,
                harvest_end,
                link,
                quantity,
                row_margin,
                plant_margin,
            } = seed;
            return {
                name,
                link,
                preGrow:
                    pre_grow_start !== null && pre_grow_end !== null
                        ? [Number(pre_grow_start), Number(pre_grow_end)]
                        : undefined,
                planting:
                    planting_start !== null && planting_end !== null
                        ? [Number(planting_start), Number(planting_end)]
                        : undefined,
                harvest:
                    harvest_start !== null && harvest_end !== null
                        ? [Number(harvest_start), Number(harvest_end)]
                        : undefined,
                quantity,
                margins: {
                    plant: Number(plant_margin),
                    row: Number(row_margin),
                },
            };
        }
    );
}
