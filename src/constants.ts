export const enum Routes {
    Editor = "/editori",
    Calendar = "/kalenteri",
    Home = "/",
    SeedList = "siemenet",
}
