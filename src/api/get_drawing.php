<?php  require "utils/connection.php";
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$result = mysqli_query($connection, "SELECT * FROM drawing_objects");

$allObjects = array();

if (mysqli_num_rows($result) > 0) {
    while($object = mysqli_fetch_assoc($result)) {
        $object["shape_data"] = json_decode($object["shape_data"]);
        $allObjects[] = $object;
    }
}
echo json_encode($allObjects);
?>