<?php
require "utils/connection.php";

if (!isset($_POST["json_data"])) {
    http_response_code(400);
    die("json_data was not set");
}

$decoded = urldecode($_POST["json_data"]);
$drawingData = mb_convert_encoding($decoded, "UTF-8");
$drawing = json_decode($drawingData);

$idsInDrawing = array();

foreach($drawing as $drawingObject) {
    $shapeData = json_encode($drawingObject->shape_data, JSON_UNESCAPED_UNICODE);
    $objectId = isset($drawingObject->id) ? $drawingObject->id : NULL;
    $parent = empty($drawingObject->parent) ? "NULL" : "'{$drawingObject->parent}'";

    $objectResult = mysqli_query(
        $connection,
        "SELECT id FROM drawing_objects WHERE id='{$objectId}'"
    );
    if (mysqli_num_rows($objectResult) > 0) {
        $idsInDrawing[] = $objectId;
        mysqli_query(
            $connection,
            "UPDATE drawing_objects
            SET name='{$drawingObject->name}', parent={$parent},
                x='{$drawingObject->x}', y='{$drawingObject->y}', orientation='{$drawingObject->orientation}',
                type='{$drawingObject->type}', shape_data='{$shapeData}'
            WHERE id='{$objectId}'"
        );
    } else {
        mysqli_query(
            $connection,
            "INSERT INTO drawing_objects (name, parent, x, y, orientation, type, shape_data)
            VALUES ('{$drawingObject->name}', {$parent},
                '{$drawingObject->x}', '{$drawingObject->y}', '{$drawingObject->orientation}',
                '{$drawingObject->type}', '{$shapeData}')"
        );
        $idsInDrawing[] = mysqli_insert_id($connection);
    }
}

$idCondition = implode(",", $idsInDrawing);
mysqli_query(
    $connection,
    "DELETE FROM drawing_objects
    WHERE id NOT IN ($idCondition)"
);
?>