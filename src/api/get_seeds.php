<?php  require "utils/connection.php";
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$result = mysqli_query($connection, "SELECT * FROM seeds");

$allSeeds = array();

if (mysqli_num_rows($result) > 0) {
    while($seed = mysqli_fetch_assoc($result)) {
        $allSeeds[] = $seed;
    }
}
echo json_encode($allSeeds);
?>