import * as React from "react";

class PointerHelper {
    private pointerEvents: Map<number, PointerEvent | React.PointerEvent>;
    private pointerDistance: number | null;
    constructor() {
        this.pointerEvents = new Map();
        this.pointerDistance = null;
    }
    public eventStarted(event: PointerEvent | React.PointerEvent) {
        this.pointerEvents.set(event.pointerId, event);
    }
    public eventEnded(event: PointerEvent | React.PointerEvent) {
        this.pointerEvents.delete(event.pointerId);
        if (!this.isZoomEventActive()) {
            this.pointerDistance = null;
        }
    }

    public getPointerCount() {
        return this.pointerEvents.size;
    }

    public isZoomEventActive() {
        return (
            this.pointerEvents.size === 2 &&
            [...this.pointerEvents.values()].some(
                (event) => event.type === "pointermove"
            )
        );
    }

    public getZoomDelta() {
        if (this.pointerEvents.size === 2) {
            const [pointer1, pointer2] = this.pointerEvents.values();
            const xDistance = pointer1.clientX - pointer2.clientX;
            const yDistance = pointer1.clientY - pointer2.clientY;
            const pointerDistance = Math.sqrt(xDistance ** 2 + yDistance ** 2);
            const previousPointerDistance = this.pointerDistance;
            this.pointerDistance = pointerDistance;
            return this.pointerDistance - previousPointerDistance;
        } else {
            return undefined;
        }
    }
}

export const PointerEventHelper = new PointerHelper();
