export const makeGlobalizeFunction = <Global, Local>(
    getLocalState: (globalState: Global) => Local
) => <SelectorArgs, Result>(
    selector: (localState: Local, args?: SelectorArgs) => Result
) => (globalState: Global, args?: SelectorArgs) => {
    return selector(getLocalState(globalState), args);
};
