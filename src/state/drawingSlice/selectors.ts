import { DrawingState } from "./drawingSlice";
import { makeGlobalizeFunction } from "../utils";
import { StoreState } from "../store";
import {
    DrawingObject,
    DrawingObjectWithNames,
    isPolygonObject,
    isRectangleObject,
    ObjectType,
    PlantObject,
    PolygonObject,
    RectangleObject,
} from "./types";
import { degToRad } from "../../utils/MathUtils";
import { getRectangleCornerPoints } from "./utils";

const globalize = makeGlobalizeFunction<StoreState, DrawingState>(
    (state) => state.drawing
);

export const getSelectedObjects = globalize((state) => state.selectection);

export const getObjects = globalize((state) =>
    Object.entries(state.objects).map(([name, data]) => ({ name, ...data }))
);

export const getObjectNames = globalize((state) => Object.keys(state.objects));

export const getLocationOffset = globalize((state, name: string) => {
    let object = state.objects[name];
    let offset = { x: 0, y: 0 };
    while (object.parent !== null) {
        object = state.objects[object.parent];
        offset.x += object.location.x;
        offset.y += object.location.y;
    }
    return offset;
});

export const getRootObjects = globalize((state) =>
    Object.keys(state.objects).filter(
        (objectName) => state.objects[objectName].parent === null
    )
);

export const getObjectPoints = globalize((state, name: string) => {
    const object = state.objects[name];
    if (isPolygonObject(object.type)) {
        return (object as PolygonObject).points;
    } else {
        throw new Error(`There is no points in shape ${object.type}`);
    }
});

export const getHeight = globalize((state, name: string) => {
    const object = state.objects[name];
    if (isRectangleObject(object.type)) {
        return (object as RectangleObject).height;
    } else {
        throw new Error(`There is no height in shape ${object.type}`);
    }
});
export const getWidth = globalize((state, name: string) => {
    const object = state.objects[name];
    if (isRectangleObject(object.type)) {
        return (object as RectangleObject).width;
    } else {
        throw new Error(`There is no height in shape ${object.type}`);
    }
});

export const getOrientation = globalize(
    (state, name: string) => state.objects[name].orientation
);

export const getPlantName = globalize((state, name: string) => {
    const object = state.objects[name];
    if (object.type === ObjectType.Plant) {
        return (object as PlantObject).plant;
    } else {
        throw new Error(`There is no plant name in ${object.type}`);
    }
});

export const getObjectLocation = globalize(
    (state, name: string) => state.objects[name].location
);

export const getObjectType = globalize(
    (state, name: string) => state.objects[name].type
);

export const getObject = globalize(
    (state, name: string) => state.objects[name]
);

export const getCopiedObjects = globalize((state) => state.copiedObjects);

export const isSelected = globalize((state, name: string) =>
    state.selectection.includes(name)
);

export const getChildren = globalize((state, parentName: string) =>
    Object.keys(state.objects).filter(
        (name) => state.objects[name].parent === parentName
    )
);

export const getZoomLevel = globalize((state) => state.zoomLevel);

export const getPanPosition = globalize((state) => state.panPosition);

export interface StructureObject {
    [key: string]: StructureObject;
}

export const getStructure = globalize((state) =>
    Object.keys(state.objects).reduce((accum, name) => {
        setChild(accum, getPath(state.objects, name));
        return accum;
    }, {} as StructureObject)
);

const setChild = (object: StructureObject, path: string[]): StructureObject => {
    const [nextPathPart, ...restPath] = path;
    const nextPart = object[nextPathPart];
    if (nextPart === undefined) {
        object[nextPathPart] = {};
    }
    if (restPath.length !== 0) {
        return setChild(object[nextPathPart], restPath);
    } else {
        return object;
    }
};

const getPath = (structure: { [key: string]: DrawingObject }, name: string) => {
    const path = [name];
    let nextPart = structure[name].parent;
    while (nextPart !== null) {
        path.unshift(nextPart);
        nextPart = structure[nextPart].parent;
    }
    return path;
};

export const getDrawingObjectList = globalize(
    (state): DrawingObjectWithNames[] =>
        Object.entries(state.objects).map(([name, data]) => ({ name, ...data }))
);

export interface SnapPoint {
    x: number;
    y: number;
    objectName: string;
    pointIndex: number;
}

export const getSnapPoints = globalize((state) => {
    return Object.keys(state.objects).reduce((snapPoints, objectName) => {
        const object = state.objects[objectName];
        if (isPolygonObject(object.type)) {
            (object as PolygonObject).points.forEach((point, index) => {
                snapPoints.push({
                    x: point.x + object.location.x,
                    y: point.y + object.location.y,
                    objectName,
                    pointIndex: index,
                });
            });
        } else {
            snapPoints.push(
                ...getRectangleCornerPoints(
                    objectName,
                    object as RectangleObject
                )
            );
        }
        return snapPoints;
    }, [] as SnapPoint[]);
});

export const getSetPlantCount = globalize((state, name: string) => {
    const plantArea = state.objects[name];
    if (plantArea.type === ObjectType.Plant) {
        return plantArea.count;
    }
});

export const getActiveSnapLines = globalize((state) => state.activeSnapLines);
