export enum ObjectType {
    Base = "Tausta",
    Path = "Polku",
    Lavakaulus = "Lavakaulus",
    Other = "Muu",
    Plant = "Kasvi",
}

export interface Coordinates {
    x: number;
    y: number;
}

interface BaseObject {
    type: ObjectType;
    parent: null | string;
    location: Coordinates;
    orientation: number;
    id?: number;
}

export interface PolygonObject extends BaseObject {
    type: PolygonObjectType;
    points: Coordinates[];
}

export interface RectangleObject extends BaseObject {
    type: ObjectType.Lavakaulus;
    height: number;
    width: number;
}

export interface PlantObject extends BaseObject {
    type: ObjectType.Plant;
    height: number;
    width: number;
    plant: string;
    count?: number;
}

export type PolygonObjectType =
    | ObjectType.Base
    | ObjectType.Other
    | ObjectType.Path;

export type RectangleObjectType = ObjectType.Lavakaulus | ObjectType.Plant;

export const isPolygonObject = (type: ObjectType) =>
    type === ObjectType.Base ||
    type === ObjectType.Other ||
    type === ObjectType.Path;

export const isRectangleObject = (type: ObjectType) =>
    type === ObjectType.Plant || type === ObjectType.Lavakaulus;

export type DrawingObject = PolygonObject | RectangleObject | PlantObject;

export type DrawingObjectWithNames = DrawingObject & { name: string };

export type DrawingObjects = { [name: string]: DrawingObject };

export const enum PlantType {
    Single,
    Area,
}

export enum Shape {
    Circle = "Ympyrä",
    Rectangle = "Suorakulmio",
    Free = "Monikulmio",
}

export const defaultLavakaulusData: RectangleObject = {
    type: ObjectType.Lavakaulus,
    height: 80,
    width: 120,
    orientation: 0,
    parent: null,
    location: { x: 100, y: 100 },
};
