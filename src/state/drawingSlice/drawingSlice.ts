import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
    DrawingObject,
    ObjectType,
    Coordinates,
    DrawingObjects,
    DrawingObjectWithNames,
    isPolygonObject,
    PolygonObject,
    isRectangleObject,
    RectangleObject,
} from "./types";

export const initialDrawingState = {
    objects: {} as DrawingObjects,
    selectection: [] as string[],
    copiedObjects: [] as string[],
    zoomLevel: 1,
    panPosition: { x: 0, y: 0 },
    activeSnapLines: { x: [] as number[], y: [] as number[] },
};

export type DrawingState = typeof initialDrawingState;

export const getDefaultAreaData = (): DrawingObject => ({
    type: ObjectType.Other,
    points: [
        { x: 0, y: 0 },
        { x: 150, y: 0 },
        { x: 150, y: 150 },
        { x: 0, y: 150 },
    ],
    location: {
        x: 100,
        y: 100,
    },
    parent: null,
    orientation: 0,
});

export const drawingSlice = createSlice({
    name: "model",
    initialState: initialDrawingState,
    reducers: {
        initializeDrawing(
            state,
            action: PayloadAction<DrawingObjectWithNames[]>
        ) {
            state.objects = Object.fromEntries(
                action.payload.map((object) => {
                    const { name, ...value } = object;
                    return [name, value];
                })
            );
        },
        addObject(state, action: PayloadAction<DrawingObject | undefined>) {
            let newObject = getDefaultAreaData();
            if (action.payload) {
                newObject = { ...newObject, ...action.payload };
            }
            newObject.parent =
                state.selectection.length !== 0 ? state.selectection[0] : null;
            state.objects[
                getNewAreaName(Object.keys(state.objects), "alue")
            ] = newObject;
        },
        removeSelectedObject(state) {
            const selectedObjects = state.selectection;
            selectedObjects.forEach((objectName) => {
                delete state.objects[objectName];
            });
            Object.values(state.objects).forEach((object) => {
                if (selectedObjects.includes(object.parent)) {
                    object.parent = null;
                }
            });
            state.selectection = [];
        },
        renameObject(
            state,
            action: PayloadAction<{ oldName: string; newName: string }>
        ) {
            let newName = getNewAreaName(
                Object.keys(state.objects),
                action.payload.newName
            );
            state.objects[newName] = state.objects[action.payload.oldName];
            delete state.objects[action.payload.oldName];
            state.selectection = [newName];
        },
        changeObjectData(
            state,
            action: PayloadAction<{
                name: string;
                newData: Partial<DrawingObject>;
            }>
        ) {
            const object = state.objects[action.payload.name];
            const newType = action.payload.newData.type || object.type;
            if (isRectangleObject(newType) && !isRectangleObject(object.type)) {
                delete (object as PolygonObject).points;
                // TODO: some conversion
                (object as RectangleObject).width = 100;
                (object as RectangleObject).height = 100;
            } else if (
                isPolygonObject(newType) &&
                !isPolygonObject(object.type)
            ) {
                delete (object as RectangleObject).width;
                delete (object as RectangleObject).height;
                (object as PolygonObject).points = [
                    { x: 0, y: 0 },
                    { x: 150, y: 0 },
                    { x: 150, y: 150 },
                    { x: 0, y: 150 },
                ];
            }
            Object.entries(action.payload.newData).forEach(([key, value]) => {
                //@ts-ignore
                object[key] = value;
            });
        },
        setOrientation(
            state,
            action: PayloadAction<{ name: string; orientation: number }>
        ) {
            state.objects[action.payload.name].orientation =
                action.payload.orientation;
        },
        setSelectedObjects(state, action: PayloadAction<string[]>) {
            state.selectection = action.payload;
        },
        moveObjectPoint(
            state,
            action: PayloadAction<{
                name: string;
                pointIndex: number;
                location: Coordinates;
            }>
        ) {
            const area = state.objects[action.payload.name];
            if (isPolygonObject(area.type)) {
                (area as PolygonObject).points[action.payload.pointIndex] =
                    action.payload.location;
            }
        },
        setRectangleDimensions(
            state,
            action: PayloadAction<{
                name: string;
                height?: number;
                width?: number;
            }>
        ) {
            const object = state.objects[action.payload.name];
            if (isRectangleObject(object.type)) {
                if (action.payload.height) {
                    (object as RectangleObject).height = action.payload.height;
                }
                if (action.payload.width) {
                    (object as RectangleObject).width = action.payload.width;
                }
            }
        },
        adjustCoordinates(state, action: PayloadAction<string>) {
            const area = state.objects[action.payload];
            if (isPolygonObject(area.type)) {
                const deltaX = (area as PolygonObject).points[0].x;
                const deltaY = (area as PolygonObject).points[0].y;
                if (deltaX !== 0 || deltaY !== 0) {
                    (area as PolygonObject).points = (area as PolygonObject).points.map(
                        (point) => ({
                            x: point.x - deltaX,
                            y: point.y - deltaY,
                        })
                    );
                    area.location = {
                        x: area.location.x + deltaX,
                        y: area.location.y + deltaY,
                    };
                }
            } else {
                throw new Error(`Can't adjust points for shape ${area.type}`);
            }
        },
        moveObject(
            state,
            action: PayloadAction<{ name: string; location: Coordinates }>
        ) {
            const object = state.objects[action.payload.name];
            object.location = action.payload.location;
        },
        setCopiedObject(state, action: PayloadAction<string[]>) {
            state.copiedObjects = action.payload;
        },
        pasteObjects(state) {
            state.copiedObjects
                .map((objectName) => state.objects[objectName])
                .map((object) => ({
                    ...object,
                    location: {
                        x: object.location.x + 10,
                        y: object.location.y + 10,
                    },
                }))
                .forEach((newObject) => {
                    state.objects[
                        getNewAreaName(Object.keys(state.objects), "alue")
                    ] = newObject;
                });
        },
        setObjectParent(
            state,
            action: PayloadAction<{ name: string; parent: string }>
        ) {
            state.objects[action.payload.name].parent = action.payload.parent;
        },
        zoomIn(state) {
            state.zoomLevel = state.zoomLevel * 1.1;
        },
        zoomOut(state) {
            state.zoomLevel = state.zoomLevel * 0.9;
        },
        setPanPosition(state, action: PayloadAction<Coordinates>) {
            state.panPosition = action.payload;
        },
        addPointToRectangle(
            state,
            action: PayloadAction<{
                name: string;
                index: number;
                location: Coordinates;
            }>
        ) {
            const { name, location, index } = action.payload;
            const object = state.objects[name];
            if (
                object.type === ObjectType.Base ||
                object.type === ObjectType.Other ||
                object.type === ObjectType.Path
            ) {
                object.points.splice(index, 0, location);
            }
        },
        changeCount(
            state,
            action: PayloadAction<{ name: string; count: number | undefined }>
        ) {
            const { name, count } = action.payload;
            const object = state.objects[name];
            if (object.type === ObjectType.Plant) {
                object.count = count;
            }
        },
        setSnapLines(
            state,
            action: PayloadAction<{ x: number[]; y: number[] }>
        ) {
            state.activeSnapLines = action.payload;
        },
        resetSnapLines(state) {
            state.activeSnapLines = { x: [], y: [] };
        },
    },
});

const getNewAreaName = (usedNames: string[], suggestedName: string) => {
    let newName = suggestedName;
    // TODO: there is probably a faster way to do this
    let postfix = 0;
    let isAllowedName = false;
    while (!isAllowedName) {
        const nameSuggestion = postfix > 0 ? newName + postfix : newName;
        if (usedNames.includes(nameSuggestion)) {
            postfix++;
        } else {
            newName = nameSuggestion;
            isAllowedName = true;
        }
    }
    return newName;
};

export const drawingActions = drawingSlice.actions;
