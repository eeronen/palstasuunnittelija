import { degToRad } from "../../utils/MathUtils";
import { SnapPoint } from "./selectors";
import { Coordinates } from "./types";

export const getRectangleCornerPoints = (
    objectName: string,
    object: {
        location: Coordinates;
        height: number;
        width: number;
        orientation: number;
    }
): SnapPoint[] => {
    const location = object.location;
    const height = object.height;
    const width = object.width;
    const orientation = degToRad(object.orientation);
    return [
        {
            x: location.x,
            y: location.y,
            objectName,
            pointIndex: 0,
        },
        {
            x: location.x + Math.cos(orientation) * width,
            y: location.y + Math.sin(orientation) * width,
            objectName,
            pointIndex: 1,
        },
        {
            x:
                location.x -
                Math.sin(orientation) * height +
                Math.cos(orientation) * width,
            y:
                location.y +
                Math.cos(orientation) * height +
                Math.sin(orientation) * width,
            objectName,
            pointIndex: 2,
        },
        {
            x: location.x - Math.sin(orientation) * height,
            y: location.y + Math.cos(orientation) * height,
            objectName,
            pointIndex: 3,
        },
    ];
};
