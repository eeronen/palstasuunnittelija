import { Action, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";
import { getRowMargin } from "../SeedsSlice/selectors";
import { store, StoreState } from "../store";
import { drawingActions } from "./drawingSlice";
import { defaultLavakaulusData, ObjectType, PlantObject } from "./types";

interface AddObjectOptions {
    plantType?: string;
}

export const addObject = (
    type?: ObjectType,
    options?: AddObjectOptions
): ThunkAction<void, StoreState, null, Action> => (dispatch, getState) => {
    if (type === ObjectType.Lavakaulus) {
        dispatch(drawingActions.addObject(defaultLavakaulusData));
    } else if (type === ObjectType.Plant) {
        const width = getRowMargin(getState(), options?.plantType || "Retiisi");
        const height = 80;
        dispatch(
            drawingActions.addObject({
                type: ObjectType.Plant,
                width,
                height,
                parent: null,
                plant: options?.plantType || "Retiisi",
                location: { x: 100, y: 100 },
                orientation: 0,
            })
        );
    } else {
        dispatch(drawingActions.addObject());
    }
};
