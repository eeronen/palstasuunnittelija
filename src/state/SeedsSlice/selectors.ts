import { SeedsState } from ".";
import { StoreState } from "../store";
import { makeGlobalizeFunction } from "../utils";

const globalize = makeGlobalizeFunction<StoreState, SeedsState>(
    (state) => state.seeds
);

export const getSeeds = globalize((state) => state.seeds);

export const getSeedNames = globalize((state) => Object.keys(state.seeds));

export const getRowMargin = globalize((state, name: string) => {
    const seed = state.seeds[name];
    if (seed) {
        return seed.margins.row;
    } else {
        return 30;
    }
});

export const getPlantMargin = globalize((state, name: string) => {
    const seed = state.seeds[name];
    if (seed) {
        return seed.margins.plant;
    } else {
        return 5;
    }
});
