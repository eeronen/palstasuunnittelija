export const initialSeedsState = {
    seeds: {} as { [name: string]: SeedData },
};

export interface SeedData {
    quantity: number;
    preGrow?: [number, number];
    planting?: [number, number];
    harvest: [number, number];
    link?: string;
    margins: {
        row: number;
        plant: number;
    };
}

export type SeedDataWithName = SeedData & { name: string };

export interface SeedTransferFormat {
    id: string;
    name: string;
    link: string;
    quantity: number;
    pre_grow_start: number | null;
    pre_grow_end: number | null;
    planting_start: number | null;
    planting_end: number | null;
    harvest_start: number;
    harvest_end: number;
    row_margin: number;
    plant_margin: number;
}
