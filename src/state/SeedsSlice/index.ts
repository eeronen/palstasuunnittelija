import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { SeedData, initialSeedsState, SeedDataWithName } from "./types";

export type SeedsState = typeof initialSeedsState;

export const seedsSlice = createSlice({
    name: "seeds",
    initialState: initialSeedsState,
    reducers: {
        initializeSeedData(state, action: PayloadAction<SeedDataWithName[]>) {
            state.seeds = Object.fromEntries(
                action.payload.map((seedData): [string, SeedData] => {
                    const { name, ...value } = seedData;
                    return [name, value];
                })
            );
        },
    },
});

export const seedActions = seedsSlice.actions;
