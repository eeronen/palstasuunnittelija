import { getDrawingObjectList, getPlantName } from "./drawingSlice/selectors";
import { ObjectType, PlantObject } from "./drawingSlice/types";
import { StoreState } from "./store";

export const getPlantCount = (state: StoreState, objectName: string) => {
    const plantArea = state.drawing.objects[objectName];
    if (plantArea.type === ObjectType.Plant) {
        if (plantArea.count) {
            return plantArea.count;
        }
        const seed = state.seeds.seeds[plantArea.plant];
        if (seed) {
            return Math.round(plantArea.height / seed.margins.plant);
        } else {
            return 0;
        }
    } else {
        return 0;
    }
};

export const getAllPlantCounts = (state: StoreState) => {
    const plantAreas = getDrawingObjectList(state).filter(
        (object) => object.type === ObjectType.Plant
    ) as (PlantObject & { name: string })[];
    return plantAreas.reduce((plantCounts, area) => {
        let count = area.count;
        if (count === undefined) {
            count = getPlantCount(state, area.name);
        }
        if (plantCounts[area.plant] === undefined) {
            plantCounts[area.plant] = count;
        } else {
            plantCounts[area.plant] = plantCounts[area.plant] + count;
        }
        return plantCounts;
    }, {} as { [name: string]: number });
};
