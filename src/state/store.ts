import { configureStore } from "@reduxjs/toolkit";
import { drawingSlice } from "./drawingSlice/drawingSlice";
import { seedsSlice } from "./SeedsSlice";

export const store = configureStore({
    reducer: { drawing: drawingSlice.reducer, seeds: seedsSlice.reducer },
});

export type StoreState = ReturnType<typeof store.getState>;
